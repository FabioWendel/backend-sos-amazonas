"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _express = require('express'); var _express2 = _interopRequireDefault(_express);

// import { resolve } from 'path';
var _youch = require('youch'); var _youch2 = _interopRequireDefault(_youch);
var _node = require('@sentry/node'); var Sentry = _interopRequireWildcard(_node);
var _cors = require('cors'); var _cors2 = _interopRequireDefault(_cors);
require('express-async-errors');

var _routes = require('./routes'); var _routes2 = _interopRequireDefault(_routes);
var _sentry = require('./config/sentry'); var _sentry2 = _interopRequireDefault(_sentry);

require('./database');

const path = require('path');

const http = require('http');
// const socketIO = require('socket.io');

class App {
  constructor() {
    this.server = _express2.default.call(void 0, );
    this.app = http.Server(this.server);
    // this.io = socketIO(this.app);

    if (process.NODE_ENV === 'production') {
      Sentry.init(_sentry2.default);
    }

    this.middlewares();
    this.routes();
    this.exceptionHandler();
  }

  middlewares() {
    if (process.NODE_ENV === 'production') {
      this.server.use(Sentry.Handlers.requestHandler());
    } else {
      // this.server.use((req, res, next) => setTimeout(next, 5000));
    }
    // this.server.use((req, res, next) => {
    //   req.io = this.io;

    //   next();
    // });

    this.server.use(_cors2.default.call(void 0, ));
    this.server.use(_express2.default.json({ limit: '2gb', extended: true }));
    this.server.use(_express2.default.urlencoded({ limit: '2gb', extended: true }));
    this.server.use(_express2.default.static(path.join(__dirname, 'angular', 'build')));
    this.server.get('/', (req, res) => {
      console.log('funfa o  angular ');
      res.sendFile(path.join(__dirname, 'angular', 'build', 'index.html'));
    });
    // this.server.use(
    //   '/files',
    //   express.static(resolve(__dirname, '..', 'tmp', 'uploads'))
    // );
  }

  routes() {
    this.server.use(_routes2.default);
    if (process.NODE_ENV === 'production') {
      this.server.use(Sentry.Handlers.errorHandler());
    }
  }

  exceptionHandler() {
    this.server.use(async (err, req, res, next) => {
      if (process.env.NODE_ENV === 'production') {
        const errors = await new (0, _youch2.default)(err, req).toJSON();

        return res.status(500).json(errors);
      }

      if (
        process.env.NODE_ENV === 'test' ||
        process.env.NODE_ENV === 'development'
      ) {
        // eslint-disable-next-line no-console
        console.log('erro =>', err);
      }

      return res.status(500).json({ error: 'Internal server error' });
    });
  }
}

exports. default = new App().app;
