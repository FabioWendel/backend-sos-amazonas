(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["layouts-admin-layout-admin-layout-module"],{

/***/ "/dgT":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cadastro/cadastro.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\n  <div class=\"container-fluid\">\n    <div class=\"d-flex\">\n      <mat-form-field>\n        <mat-label>Procurar</mat-label>\n        <input matInput [disabled]=\"isLoading\" (keyup)=\"applyFilter($event)\" placeholder=\"Procurar...\" #input>\n      </mat-form-field>\n      <button mat-mini-fab color=\"warn\" (click)=\"openDialog()\" class=\"ml-2 mt-3 custom-icon-add\">\n        <mat-icon>person_add_alt_1</mat-icon>\n      </button>\n    </div>\n\n    <div class=\"example-container\">\n\n      <div class=\"example-loading-shade\" *ngIf=\"isLoading\">\n        <mat-spinner *ngIf=\"isLoading\" [diameter]=\"35\" color=\"warn\"></mat-spinner>\n      </div>\n\n      <div class=\"example-table-container mat-elevation-z8\">\n\n        <table mat-table [dataSource]=\"cadastros\" matSort>\n\n          <ng-container matColumnDef=\"termo\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Termo </th>\n            <td mat-cell *matCellDef=\"let row\"> {{row.termo}} </td>\n          </ng-container>\n\n          <ng-container matColumnDef=\"municipio\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Município </th>\n            <td mat-cell *matCellDef=\"let row\"> {{row.municipio}} </td>\n          </ng-container>\n\n          <ng-container matColumnDef=\"local\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Local </th>\n            <td mat-cell *matCellDef=\"let row\"> {{row.local}} </td>\n          </ng-container>\n          <!-- \n          <ng-container matColumnDef=\"item\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Items </th>\n            <td mat-cell *matCellDef=\"let row\"> {{row.items.length}} </td>\n          </ng-container> -->\n          <!-- \n          <ng-container matColumnDef=\"quantidade\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Quantidade de items </th>\n            <td mat-cell *matCellDef=\"let row\"> {{row.items}} </td>\n          </ng-container> -->\n\n          <ng-container matColumnDef=\"data\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Data </th>\n            <td mat-cell *matCellDef=\"let row\"> {{row.data | slice: 0:10 | date: 'dd/MM/yyyy'}} </td>\n          </ng-container>\n\n\n          <ng-container matColumnDef=\"opcao\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Opções </th>\n            <td mat-cell *matCellDef=\"let row let i = index\">\n              <button mat-mini-fab color=\"warn\" (click)=\"openDialogEdit(cadastros.data[i])\" class=\"my-fab\">\n                <mat-icon>edit</mat-icon>\n              </button>\n            </td>\n          </ng-container>\n\n          <tr mat-header-row *matHeaderRowDef=\"displayedColumns;\"></tr>\n          <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n\n          <tr class=\"mat-row\" *matNoDataRow>\n            <td class=\"mat-cell\" colspan=\"4\">Nenhum dado corresponde ao filtro \"{{input.value}}\"</td>\n          </tr>\n        </table>\n        <mat-paginator [length]=\"totalPages\" [pageSize]=\"5\" (page)=\"getCurrent($event)\"></mat-paginator>\n      </div>\n\n    </div>\n  </div>\n</div>\n\n<ng-template #myTemplate>\n  <mat-dialog-content class=\"mat-typography\">\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n        <div class=\"card\">\n          <div class=\"card-header card-header-danger\">\n            <h4 class=\"card-title\">Cadastro</h4>\n            <p class=\"card-category\">Cadastrar informação da doação</p>\n          </div>\n          <div class=\"card-body\">\n            <form [formGroup]=\"userform\" (ngSubmit)=\"onSubmit()\">\n              <div class=\"row\">\n                <div class=\"col-md-4\">\n                  <mat-form-field class=\"example-full-width\">\n                    <input matInput placeholder=\"Número Termo *\" formControlName=\"termo\">\n                  </mat-form-field>\n                </div>\n                <div class=\"col-md-4\">\n                  <mat-form-field>\n                    <mat-select placeholder=\"Município\" formControlName=\"municipio\" required>\n                      <mat-select-filter [placeholder]=\"'Procurar'\" [displayMember]=\"'nome'\" [array]=\"municipios\"\n                        (filteredReturn)=\"filteredListMunicipios =$event\"></mat-select-filter>\n                      <mat-option *ngFor=\"let municipio of filteredListMunicipios\" [value]=\"municipio.nome\">\n                        {{municipio.nome}}\n                      </mat-option>\n                    </mat-select>\n                  </mat-form-field>\n                  <!-- <mat-form-field class=\"example-full-width\">\n                    <input matInput placeholder=\"Município *\" formControlName=\"municipio\">\n                  </mat-form-field> -->\n                </div>\n                <div class=\"col-md-4\">\n                  <mat-form-field class=\"example-full-width\">\n                    <input matInput placeholder=\"Data *\" type=\"date\" formControlName=\"data\">\n                  </mat-form-field>\n                </div>\n              </div>\n              <div class=\"row\">\n                <!-- <div class=\"col-md-4\">\n                  <mat-form-field class=\"example-full-width\">\n                    <input matInput placeholder=\"Quantidade *\" type=\"number\" formControlName=\"quantidade\">\n                  </mat-form-field>\n                </div> -->\n                <!-- <div class=\"col-md-6\">\n                  <mat-form-field class=\"example-full-width\">\n                    <input matInput placeholder=\"Unidade de Medida *\" type=\"text\" formControlName=\"medida\">\n                  </mat-form-field>\n                </div> -->\n                <div class=\"col-md-6\">\n                  <mat-form-field class=\"example-full-width\">\n                    <input matInput placeholder=\"Local *\" type=\"text\" formControlName=\"local\">\n                  </mat-form-field>\n                </div>\n                <div class=\"col-md-6\">\n                  <mat-form-field>\n                    <ngx-mat-file-input type=\"file\" accept=\"image/*,application/pdf\" formControlName=\"evidencia\"\n                      placeholder=\"Anexar evidência\" (change)=\"onFileChange($event.target.files)\">\n                      <mat-icon ngxMatFileInputIcon>folder</mat-icon>\n                    </ngx-mat-file-input>\n                  </mat-form-field>\n                </div>\n              </div>\n              <!-- <div class=\"row\">\n                <div class=\"col-md-12\">\n                  <mat-form-field class=\"example-full-width\">\n                    <input matInput placeholder=\"Local *\" type=\"text\" formControlName=\"local\">\n                  </mat-form-field>\n                </div>\n                <div class=\"col-md-4\">\n                  <mat-form-field>\n                    <ngx-mat-file-input type=\"file\" accept=\"image/*,application/pdf\" formControlName=\"evidencia\"\n                      placeholder=\"Anexar evidência\" (change)=\"onFileChange($event.target.files)\">\n                      <mat-icon matSuffix>folder</mat-icon>\n                    </ngx-mat-file-input>\n                  </mat-form-field>\n                </div>\n              </div> -->\n              <div class=\"row mb-2\">\n                <div class=\"col-md-12\">\n                  <button type=\"button\" mat-raised-button color=\"accent\" (click)=\"addItem()\"\n                    class=\"custom-icon-add-item\">\n                    <mat-icon>post_add</mat-icon>item\n                  </button>\n                </div>\n              </div>\n              <div class=\"row\" formArrayName=\"items\">\n                <div class=\"col-md-12\" *ngFor=\"let item of items().controls; let i=index\" [formGroupName]=\"i\">\n                  <!-- <mat-form-field id=\"input-item\" class=\"example-full-width\">\n                    <input matInput placeholder=\"Item *\" type=\"text\" formControlName=\"item\">\n                  </mat-form-field> -->\n                  <div class=\"row\">\n                    <div class=\"col-md-4\">\n                      <mat-form-field>\n                        <mat-label>Item</mat-label>\n                        <mat-select #select formControlName=\"name\" (click)=\"handleClickSelect(i)\" required>\n                          <mat-select-filter *ngIf=\"select.focused\" [placeholder]=\"'Procurar'\" [displayMember]=\"'value'\" [hasGroup]=\"true\" [groupArrayName]=\"'items'\" [array]=\"filteredListItems\"\n                          (filteredReturn)=\"inputFilterResert($event, i)\"></mat-select-filter>\n                          <mat-optgroup *ngFor=\"let group of (filterInputRow[i] || filteredListItems)\" [label]=\"group.nome\">\n                            <mat-option style=\"height: unset;  white-space: normal; border-bottom: 1px solid #e3e8ef;\"  *ngFor=\"let item of group.items\" [value]=\"item.value\">\n                              {{item.viewValue}}\n                            </mat-option>\n                          </mat-optgroup>\n                        </mat-select>\n                      </mat-form-field>\n                    </div>\n                    <div class=\"col-md-4\">\n                      <!-- <mat-form-field class=\"example-full-width\">\n                        <input matInput placeholder=\"Unidade de Medida *\" type=\"text\" formControlName=\"medida\">\n                      </mat-form-field> -->\n                      <mat-form-field>\n                        <mat-select placeholder=\"Unidade de Medida\" formControlName=\"medida\" required>\n                          <mat-option *ngFor=\"let medida of unidadeDeMedida\" [value]=\"medida.value\">\n                            {{medida.value}}\n                          </mat-option>\n                        </mat-select>\n                      </mat-form-field>\n                    </div>\n                    <div class=\"col-md-3\">\n                        <mat-form-field class=\"example-full-width\">\n                          <input matInput placeholder=\"Quantidade *\" type=\"number\" formControlName=\"quantidade\">\n                        </mat-form-field>\n                    </div>\n                    <div class=\"col-md-1\">\n                      <button *ngIf=\"i > 0\" mat-icon-button color=\"warn\" (click)=\"removeItem(i)\"\n                        class=\"custom-icon-remove\">\n                        <mat-icon>delete_outline</mat-icon>\n                      </button>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"row\">\n                <div class=\"col-md-12\">\n                  <mat-form-field class=\"example-full-width\">\n                    <textarea matInput placeholder=\"Observação...\" formControlName=\"observacao\"></textarea>\n                  </mat-form-field>\n                </div>\n              </div>\n              <div class=\"row button-cadastro-flutuante\">\n                <button mat-raised-button color=\"accent\" type=\"submit\" class=\"btn pull-right\">Cadastrar\n                  <mat-icon>save</mat-icon>\n                </button>\n                <button type=\"button\" *ngIf=\"controlDelete\" (click)=\"deleteCadastro()\" mat-raised-button\n                  class=\"btn btn-danger pull-right\">Delete\n                  <mat-icon>delete_outline</mat-icon>\n                </button>\n              </div>\n              <div class=\"row\">\n                <div class=\"col-md-12\">\n                  <div *ngIf=\"controlPreviewFormat\">\n                    <pdf-viewer [src]=\"imgSrc\" height=\"200\" [render-text]=\"true\" style=\"display: block;\"></pdf-viewer>\n                  </div>\n                  <div *ngIf=\"!controlPreviewFormat\">\n                    <img src=\"{{imgSrc}}\" class=\"img-cadastro-moble\" height=\"200\" alt=\"Pré-visualização...\"\n                      *ngIf=\"imgSrc\">\n                  </div>\n                </div>\n              </div>\n              <!-- <button mat-raised-button color=\"accent\" type=\"submit\" class=\"btn pull-right\">Cadastrar\n                <mat-icon>save</mat-icon>\n              </button>\n              <button type=\"button\" *ngIf=\"controlDelete\" (click)=\"deleteCadastro()\" mat-raised-button\n                class=\"btn btn-danger pull-right\">Delete\n                <mat-icon>delete_outline</mat-icon>\n              </button> -->\n              <div class=\"clearfix\"></div>\n            </form>\n          </div>\n        </div>\n      </div>\n    </div>\n  </mat-dialog-content>\n</ng-template>");

/***/ }),

/***/ "0cW3":
/*!****************************************************************!*\
  !*** ./src/app/shared/services/dashboard/dashboard.service.ts ***!
  \****************************************************************/
/*! exports provided: DashboardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardService", function() { return DashboardService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! environments/environment */ "AytR");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DashboardService = /** @class */ (function () {
    function DashboardService(httpClient) {
        this.httpClient = httpClient;
        this.apiUrl = environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].dataBaseConfig.databaseURL;
    }
    DashboardService.prototype.getItems = function () {
        return this.httpClient.get(this.apiUrl + "items")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(2), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    DashboardService.prototype.getItemsMunicipio = function (municipio) {
        return this.httpClient.get(this.apiUrl + "items/" + municipio)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(2), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    DashboardService.prototype.getAllLocal = function () {
        return this.httpClient.get(this.apiUrl + "local")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(2), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    DashboardService.prototype.getByLocal = function (local) {
        return this.httpClient.get(this.apiUrl + "local/" + local)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(2), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    DashboardService.prototype.getAllItem = function () {
        return this.httpClient.get(this.apiUrl + "allitems")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(2), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    DashboardService.prototype.getByItem = function (item) {
        return this.httpClient.get(this.apiUrl + "allitems/" + item)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(2), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    DashboardService.prototype.handleError = function (error) {
        var errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        }
        else {
            errorMessage = "C\u00F3digo do erro: " + error.status + ", " + ("menssagem: " + error.message);
        }
        console.log(errorMessage);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(errorMessage);
    };
    ;
    DashboardService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] }
    ]; };
    DashboardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]])
    ], DashboardService);
    return DashboardService;
}());



/***/ }),

/***/ "7igb":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".chartWrapper {\n  position: relative;\n  overflow-x: scroll;\n}\n\n.chartWrapper > canvas {\n  position: absolute;\n  left: 0;\n  top: 0;\n  pointer-events: none;\n  width: 100%;\n}\n\n.chartAreaWrapper {\n  width: 1500px;\n}\n\n.example-container {\n  height: 400px;\n  overflow: auto;\n}\n\ntable {\n  width: 100%;\n}\n\n.example-loading-shade {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 40px;\n  right: 0;\n  z-index: 1;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\ntd.mat-column-star {\n  width: 20px;\n  padding-right: 8px !important;\n}\n\n.mat-row:hover {\n  background-color: #cbe2cc;\n}\n\n.mat-row a {\n  color: #3e75ff;\n  text-decoration: none;\n}\n\n.mat-row a:hover {\n  text-decoration: underline;\n}\n\n.mat-form-field {\n  font-size: 14px;\n  width: 100%;\n}\n\n@media screen and (max-width: 960px) {\n  .mat-elevation-z8 {\n    background: transparent;\n    box-shadow: none;\n  }\n\n  .mat-header-row {\n    display: none;\n  }\n\n  tbody {\n    display: block;\n    width: 100%;\n  }\n\n  .mat-table {\n    background: transparent;\n  }\n  .mat-table * {\n    box-sizing: border-box;\n  }\n  .mat-table .mat-row {\n    display: block;\n    overflow: hidden;\n    height: auto;\n    position: relative;\n    clear: both;\n    box-shadow: 0 2px 1px -1px rgba(0, 0, 0, 0.2), 0 1px 1px 0 rgba(0, 0, 0, 0.14), 0 1px 3px 0 rgba(0, 0, 0, 0.12);\n    border-radius: 3px;\n  }\n  .mat-table .mat-row + .mat-row {\n    margin-top: 24px;\n  }\n  .mat-table .mat-cell {\n    display: block;\n    width: 100%;\n    padding: 0 16px;\n    margin: 16px 0;\n    border: 0 none;\n  }\n  .mat-table .mat-cell:first-child {\n    padding: 0 48px 0 16px;\n  }\n  .mat-table .mat-cell:first-child a {\n    font-size: 20px;\n    color: inherit;\n  }\n  .mat-table .mat-cell:first-child:before {\n    display: none;\n  }\n  .mat-table .mat-cell.m-card-sub-title {\n    margin-top: -8px;\n    padding: 0 48px 0 16px;\n    color: rgba(0, 0, 0, 0.54);\n  }\n  .mat-table .has_label_on_mobile:before {\n    content: attr(data-label);\n    display: inline;\n    font-weight: normal;\n  }\n  .mat-table .mat-column-star {\n    width: auto;\n    padding: 8px 0 0 !important;\n    margin: 0;\n    position: absolute;\n    top: 0;\n    right: 0;\n  }\n  .mat-table .mat-column-star:before {\n    display: none;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0Usa0JBQUE7QUFDSjs7QUFFQTtFQUNFLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7RUFDQSxvQkFBQTtFQUNBLFdBQUE7QUFDRjs7QUFFQTtFQUNFLGFBQUE7QUFDRjs7QUFFQTtFQUNFLGFBQUE7RUFDQSxjQUFBO0FBQ0Y7O0FBRUE7RUFDRSxXQUFBO0FBQ0Y7O0FBRUE7RUFDRSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtFQUNBLFFBQUE7RUFFQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFBRjs7QUFNQTtFQUNFLFdBQUE7RUFDQSw2QkFBQTtBQUhGOztBQU9FO0VBQ0UseUJBQUE7QUFKSjs7QUFNRTtFQUNFLGNBQUE7RUFDQSxxQkFBQTtBQUpKOztBQUtJO0VBQ0UsMEJBQUE7QUFITjs7QUFRQTtFQUNFLGVBQUE7RUFDQSxXQUFBO0FBTEY7O0FBUUE7RUFDRTtJQUNFLHVCQUFBO0lBQ0EsZ0JBQUE7RUFMRjs7RUFRQTtJQUNFLGFBQUE7RUFMRjs7RUFRQTtJQUNFLGNBQUE7SUFDQSxXQUFBO0VBTEY7O0VBUUE7SUFDRSx1QkFBQTtFQUxGO0VBTUU7SUFDRSxzQkFBQTtFQUpKO0VBT0U7SUFDRSxjQUFBO0lBQ0EsZ0JBQUE7SUFDQSxZQUFBO0lBQ0Esa0JBQUE7SUFDQSxXQUFBO0lBQ0EsK0dBQUE7SUFDQSxrQkFBQTtFQUxKO0VBTUk7SUFDRSxnQkFBQTtFQUpOO0VBUUU7SUFDRSxjQUFBO0lBQ0EsV0FBQTtJQUNBLGVBQUE7SUFDQSxjQUFBO0lBQ0EsY0FBQTtFQU5KO0VBT0k7SUFDRSxzQkFBQTtFQUxOO0VBTU07SUFDRSxlQUFBO0lBRUEsY0FBQTtFQUxSO0VBT007SUFDRSxhQUFBO0VBTFI7RUFRSTtJQUNFLGdCQUFBO0lBQ0Esc0JBQUE7SUFDQSwwQkFBQTtFQU5OO0VBV0k7SUFDRSx5QkFBQTtJQUNBLGVBQUE7SUFDQSxtQkFBQTtFQVROO0VBYUU7SUFDRSxXQUFBO0lBQ0EsMkJBQUE7SUFDQSxTQUFBO0lBQ0Esa0JBQUE7SUFDQSxNQUFBO0lBQ0EsUUFBQTtFQVhKO0VBWUk7SUFDRSxhQUFBO0VBVk47QUFDRiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2hhcnRXcmFwcGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIG92ZXJmbG93LXg6IHNjcm9sbDtcbn1cblxuLmNoYXJ0V3JhcHBlciA+IGNhbnZhcyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgdG9wOiAwO1xuICBwb2ludGVyLWV2ZW50czogbm9uZTtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5jaGFydEFyZWFXcmFwcGVyIHtcbiAgd2lkdGg6IDE1MDBweDtcbn1cblxuLmV4YW1wbGUtY29udGFpbmVyIHtcbiAgaGVpZ2h0OiA0MDBweDtcbiAgb3ZlcmZsb3c6IGF1dG87XG59XG5cbnRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5leGFtcGxlLWxvYWRpbmctc2hhZGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgYm90dG9tOiA0MHB4O1xuICByaWdodDogMDtcbiAgLy8gYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjE1KTtcbiAgei1pbmRleDogMTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cblxuLy9WVlZWVlZWVlYgUmVzcG9uc2l2ZSBDU1MgTW9iaWxlIEZhYmlvIFdlbmRlbCBWVlZWVlZWVlZWXG5cbnRkLm1hdC1jb2x1bW4tc3RhciB7XG4gIHdpZHRoOiAyMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiA4cHggIWltcG9ydGFudDtcbn1cblxuLm1hdC1yb3cge1xuICAmOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2JlMmNjO1xuICB9XG4gIGEge1xuICAgIGNvbG9yOiAjM2U3NWZmO1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAmOmhvdmVyIHtcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuICAgIH1cbiAgfVxufVxuXG4ubWF0LWZvcm0tZmllbGQge1xuICBmb250LXNpemU6IDE0cHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5NjBweCkge1xuICAubWF0LWVsZXZhdGlvbi16OCB7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgfVxuXG4gIC5tYXQtaGVhZGVyLXJvdyB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIHRib2R5IHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuXG4gIC5tYXQtdGFibGUge1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgICoge1xuICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICB9XG5cbiAgICAubWF0LXJvdyB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICBjbGVhcjogYm90aDtcbiAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDFweCAtMXB4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMCAxcHggMXB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAxcHggM3B4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICAgICsgLm1hdC1yb3cge1xuICAgICAgICBtYXJnaW4tdG9wOiAyNHB4O1xuICAgICAgfVxuICAgIH1cblxuICAgIC5tYXQtY2VsbCB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgcGFkZGluZzogMCAxNnB4O1xuICAgICAgbWFyZ2luOiAxNnB4IDA7XG4gICAgICBib3JkZXI6IDAgbm9uZTtcbiAgICAgICY6Zmlyc3QtY2hpbGQge1xuICAgICAgICBwYWRkaW5nOiAwIDQ4cHggMCAxNnB4O1xuICAgICAgICBhIHtcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgLy8gZm9udC13ZWlnaHQ6IDQwMDtcbiAgICAgICAgICBjb2xvcjogaW5oZXJpdDtcbiAgICAgICAgfVxuICAgICAgICAmOmJlZm9yZSB7XG4gICAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgJi5tLWNhcmQtc3ViLXRpdGxlIHtcbiAgICAgICAgbWFyZ2luLXRvcDogLThweDtcbiAgICAgICAgcGFkZGluZzogMCA0OHB4IDAgMTZweDtcbiAgICAgICAgY29sb3I6IHJnYmEoMCwgMCwgMCwgMC41NCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLmhhc19sYWJlbF9vbl9tb2JpbGUge1xuICAgICAgJjpiZWZvcmUge1xuICAgICAgICBjb250ZW50OiBhdHRyKGRhdGEtbGFiZWwpO1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmU7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLm1hdC1jb2x1bW4tc3RhciB7XG4gICAgICB3aWR0aDogYXV0bztcbiAgICAgIHBhZGRpbmc6IDhweCAwIDAgIWltcG9ydGFudDtcbiAgICAgIG1hcmdpbjogMDtcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIHRvcDogMDtcbiAgICAgIHJpZ2h0OiAwO1xuICAgICAgJjpiZWZvcmUge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgICAgfVxuICAgIH1cbiAgfVxufSJdfQ== */");

/***/ }),

/***/ "H/d9":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card card-chart\">\n                    <div class=\"card-header card-header-success\" style=\"background: #cbe2cc !important;\">\n                        <!-- <div class=\"ct-chart\" id=\"dailySalesChart\"></div> -->\n                        <div *ngIf=\"isLoading\" class=\"d-flex justify-content-center\" style=\"margin-top: 20%;\">\n                            <div class=\"spinner-border\" role=\"status\" style=\"width: 3rem; height: 3rem;\">\n                                <span class=\"sr-only\">Loading...</span>\n                            </div>\n                        </div>\n                        <div [style.visibility]=\"isLoading ? 'hidden' : 'visible'\" class=\"chartWrapper\">\n                            <div class=\"chartAreaWrapper\">\n                                <canvas id=\"myChartGeral\" width=\"700\" height=\"210\" #myChartGeral></canvas>\n                            </div>\n                            <canvas id=\"chartAxisGeral\" height=\"400\" width=\"0\"></canvas>\n                        </div>\n                    </div>\n                    <div class=\"card-body\">\n                        <h4 class=\"card-title\">Gráfico Geral</h4>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"stats\">\n                            <i class=\"material-icons\">access_time</i> <b>Atualizado em {{timeDate | date:\n                                \"HH:mm:ss\"}}</b>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n\n        <!-- <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card card-chart\">\n                    <div class=\"card-header card-header-success\" style=\"background: #cbe2cc !important;\">\n                        <mat-form-field appearance=\"fill\">\n                            <mat-label>Selecione o município</mat-label>\n                            <mat-select (selectionChange)=\"getItemsMunicipio($event.value)\">\n                                <mat-option *ngFor=\"let municipio of labelsMunicipios\" [value]=\"municipio\">\n                                    {{municipio}}\n                                </mat-option>\n                            </mat-select>\n                        </mat-form-field>\n                        <div *ngIf=\"isMessageSelectedMunicipio\" class=\"d-flex justify-content-center\">\n                            <h2 style=\"color: #636262;\">\n                                Selecione o município que deseja filtrar...\n                            </h2>\n                        </div>\n                        <div *ngIf=\"isLoadingItems&&!isMessageSelectedMunicipio\" class=\"d-flex justify-content-center\"\n                            style=\"margin-top: 20%;\">\n                            <div class=\"spinner-border\" role=\"status\" style=\"width: 3rem; height: 3rem;\">\n                                <span class=\"sr-only\">Loading...</span>\n                            </div>\n                        </div>\n                        <div [style.visibility]=\"isLoadingItems ? 'hidden' : 'visible'\" class=\"chartWrapper\">\n                            <div class=\"chartAreaWrapper\">\n                                <canvas id=\"myChartItems\" width=\"5000\" height=\"1500\" #myChartItems></canvas>\n                            </div>\n                            <canvas id=\"chartAxisItems\" height=\"400\" width=\"0\"></canvas>\n                        </div>\n                    </div>\n                    <div class=\"card-body\">\n                        <h4 class=\"card-title\">Gráfico Tipo de Items</h4>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"stats\">\n                            <i class=\"material-icons\">access_time</i> <b>Atualizado em {{timeDate | date:\n                                \"HH:mm:ss\"}}</b>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div> -->\n\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-warning\" style=\"background: #c88c6c !important;\">\n                        <mat-form-field appearance=\"fill\">\n                            <!-- <mat-label>Selecione o município</mat-label>\n                            <mat-select (selectionChange)=\"getItemsMunicipio($event.value)\">\n                                <mat-option *ngFor=\"let municipio of labelsMunicipios\" [value]=\"municipio\">\n                                    {{municipio}}\n                                </mat-option>\n                            </mat-select> -->\n                            <mat-select (selectionChange)=\"getItemsMunicipio($event.value)\"\n                                placeholder=\"Selecione o município\">\n                                <mat-select-filter [placeholder]=\"'Procurar'\" [displayMember]=\"\"\n                                    [array]=\"labelsMunicipios\" (filteredReturn)=\"filteredListMunicipios =$event\">\n                                </mat-select-filter>\n                                <mat-option *ngFor=\"let municipio of filteredListMunicipios\" [value]=\"municipio\">\n                                    {{municipio}}\n                                </mat-option>\n                            </mat-select>\n                        </mat-form-field>\n                        <h4 class=\"card-title\" style=\"font-weight: bold;\">{{selectedMunicipio ? selectedMunicipio :\n                            'Selecione o município que deseja filtrar acima...'}}</h4>\n                        <p class=\"card-category\" style=\"font-weight: bold;\">Consultar município que receberam doações\n                        </p>\n                    </div>\n                    <div *ngIf=\"isLoadingItems&&selectedMunicipio\" class=\"d-flex justify-content-center\"\n                        style=\"margin-top: 5%;\">\n                        <div class=\"spinner-border\" role=\"status\" style=\"width: 3rem; height: 3rem;\">\n                            <span class=\"sr-only\">Loading...</span>\n                        </div>\n                    </div>\n                    <div *ngIf=\"!isLoadingItems\" class=\"card-body table-responsive\">\n                        <div class=\"col-12\">\n                            <mat-form-field>\n                                <mat-label>Procurar</mat-label>\n                                <input matInput [disabled]=\"isLoadingItems\" (keyup)=\"applyFilter($event)\"\n                                    placeholder=\"Procurar...\" #input>\n                            </mat-form-field>\n                        </div>\n\n                        <div class=\"example-container\">\n\n                            <div class=\"example-table-container mat-elevation-z8\">\n\n                                <table mat-table [dataSource]=\"dataItemsTable\" matSort>\n\n                                    <ng-container matColumnDef=\"name\">\n                                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Nome </th>\n                                        <td mat-cell *matCellDef=\"let row\"> {{row.name}} </td>\n                                    </ng-container>\n\n                                    <ng-container matColumnDef=\"local\">\n                                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Local </th>\n                                        <td mat-cell *matCellDef=\"let row\">{{row.local}} </td>\n                                    </ng-container>\n\n                                    <ng-container matColumnDef=\"medida\">\n                                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Unidade de medida </th>\n                                        <td mat-cell *matCellDef=\"let row\"> {{row.medida}} </td>\n                                    </ng-container>\n\n                                    <ng-container matColumnDef=\"totalItems\">\n                                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Quantidade </th>\n                                        <td mat-cell *matCellDef=\"let row\"> {{row.totalItems}} </td>\n                                    </ng-container>\n\n                                    <tr mat-header-row *matHeaderRowDef=\"displayedColumns;sticky: true\"></tr>\n                                    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n\n                                    <tr class=\"mat-row\" *matNoDataRow>\n                                        <td class=\"mat-cell\" colspan=\"4\">Nenhum dado corresponde ao filtro\n                                            \"{{input.value}}\"</td>\n                                    </tr>\n                                </table>\n                            </div>\n\n                        </div>\n\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"stats\">\n                            <i class=\"material-icons\">access_time</i> <b>Atualizado em {{timeDate | date:\n                                \"HH:mm:ss\"}}</b>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-warning\" style=\"background: #6896c1 !important\">\n                        <mat-form-field appearance=\"fill\">\n                            <!-- <mat-label>Selecione o local</mat-label>\n                            <mat-select (selectionChange)=\"getItemsByLocal($event.value)\">\n                                <mat-option *ngFor=\"let local of labelsLocais\" [value]=\"local.local\">\n                                    {{local.local}}\n                                </mat-option>\n                            </mat-select> -->\n                            <mat-select (selectionChange)=\"getItemsByLocal($event.value)\"\n                                placeholder=\"Selecione o local\">\n                                <mat-select-filter [placeholder]=\"'Procurar'\" [displayMember]=\"'local'\"\n                                    [array]=\"labelsLocais\" (filteredReturn)=\"filteredListLocal =$event\">\n                                </mat-select-filter>\n                                <mat-option *ngFor=\"let local of filteredListLocal\" [value]=\"local.local\">\n                                    {{local.local}}\n                                </mat-option>\n                            </mat-select>\n                        </mat-form-field>\n                        <h4 class=\"card-title\" style=\"font-weight: bold;\">{{selectedLocal ? selectedLocal : 'Selecione o\n                            local que deseja filtrar acima...'}}</h4>\n                        <p class=\"card-category\" style=\"font-weight: bold;\">Consultar locais que receberam doações</p>\n                    </div>\n                    <div *ngIf=\"isLoadingLocais&&selectedLocal\" class=\"d-flex justify-content-center\"\n                        style=\"margin-top: 5%;\">\n                        <div class=\"spinner-border\" role=\"status\" style=\"width: 3rem; height: 3rem;\">\n                            <span class=\"sr-only\">Loading...</span>\n                        </div>\n                    </div>\n                    <div *ngIf=\"!isLoadingLocais\" class=\"card-body table-responsive\">\n                        <div class=\"col-12\">\n                            <mat-form-field>\n                                <mat-label>Procurar</mat-label>\n                                <input matInput [disabled]=\"isLoadingLocais\" (keyup)=\"applyFilterLocal($event)\"\n                                    placeholder=\"Procurar...\" #input>\n                            </mat-form-field>\n                        </div>\n\n                        <div class=\"example-container\">\n\n                            <div class=\"example-table-container mat-elevation-z8\">\n\n                                <table mat-table [dataSource]=\"dataItemsTableLocais\" matSort>\n\n                                    <ng-container matColumnDef=\"name\">\n                                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Nome </th>\n                                        <td mat-cell *matCellDef=\"let row\"> {{row.name}} </td>\n                                    </ng-container>\n\n                                    <ng-container matColumnDef=\"municipio\">\n                                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Município </th>\n                                        <td mat-cell *matCellDef=\"let row\">{{row.municipio}} </td>\n                                    </ng-container>\n\n                                    <ng-container matColumnDef=\"medida\">\n                                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Unidade de medida </th>\n                                        <td mat-cell *matCellDef=\"let row\"> {{row.medida}} </td>\n                                    </ng-container>\n\n                                    <ng-container matColumnDef=\"totalItems\">\n                                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Quantidade </th>\n                                        <td mat-cell *matCellDef=\"let row\"> {{row.totalItems}} </td>\n                                    </ng-container>\n\n                                    <tr mat-header-row *matHeaderRowDef=\"displayedColumnsLocal;sticky: true\"></tr>\n                                    <tr mat-row *matRowDef=\"let row; columns: displayedColumnsLocal;\"></tr>\n\n                                    <tr class=\"mat-row\" *matNoDataRow>\n                                        <td class=\"mat-cell\" colspan=\"4\">Nenhum dado corresponde ao filtro\n                                            \"{{input.value}}\"</td>\n                                    </tr>\n                                </table>\n                            </div>\n\n                        </div>\n\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"stats\">\n                            <i class=\"material-icons\">access_time</i> <b>Atualizado em {{timeDate | date:\n                                \"HH:mm:ss\"}}</b>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-warning\" style=\"background: #269cad !important\">\n                        <mat-form-field appearance=\"fill\">\n                            <!-- <mat-label>Selecione o item</mat-label>\n                            <mat-select (selectionChange)=\"getByLocalItems($event.value)\">\n                                <mat-option *ngFor=\"let item of labelsItems\" [value]=\"item.name\">\n                                    {{item.name}}\n                                </mat-option>\n                            </mat-select> -->\n                            <mat-select (selectionChange)=\"getByLocalItems($event.value)\"\n                            placeholder=\"Selecione o item\">\n                            <mat-select-filter [placeholder]=\"'Procurar'\" [displayMember]=\"'name'\"\n                                [array]=\"labelsItems\" (filteredReturn)=\"filteredListItems =$event\">\n                            </mat-select-filter>\n                            <mat-option *ngFor=\"let item of filteredListItems\" [value]=\"item.name\">\n                                {{item.name}}\n                            </mat-option>\n                        </mat-select>\n                        </mat-form-field>\n                        <h4 class=\"card-title\" style=\"font-weight: bold;\">{{selectedItem ? selectedItem : 'Selecione o\n                            item que deseja filtrar acima...'}}</h4>\n                        <p class=\"card-category\" style=\"font-weight: bold;\">Consultar locais que receberam doações do\n                            item selecionado</p>\n                    </div>\n                    <div *ngIf=\"isLoadingItem&&selectedItem\" class=\"d-flex justify-content-center\"\n                        style=\"margin-top: 5%;\">\n                        <div class=\"spinner-border\" role=\"status\" style=\"width: 3rem; height: 3rem;\">\n                            <span class=\"sr-only\">Loading...</span>\n                        </div>\n                    </div>\n                    <div *ngIf=\"!isLoadingItem\" class=\"card-body table-responsive\">\n                        <div class=\"col-12\">\n                            <mat-form-field>\n                                <mat-label>Procurar</mat-label>\n                                <input matInput [disabled]=\"isLoadingItem\" (keyup)=\"applyFilterItems($event)\"\n                                    placeholder=\"Procurar...\" #input>\n                            </mat-form-field>\n                        </div>\n\n                        <div class=\"example-container\">\n\n                            <div class=\"example-table-container mat-elevation-z8\">\n\n                                <table mat-table [dataSource]=\"dataItemsTableItems\" matSort>\n\n                                    <ng-container matColumnDef=\"local\">\n                                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Local </th>\n                                        <td mat-cell *matCellDef=\"let row\">{{row.local}} </td>\n                                    </ng-container>\n\n                                    <ng-container matColumnDef=\"municipio\">\n                                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Município </th>\n                                        <td mat-cell *matCellDef=\"let row\">{{row.municipio}} </td>\n                                    </ng-container>\n\n                                    <ng-container matColumnDef=\"medida\">\n                                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Unidade de medida </th>\n                                        <td mat-cell *matCellDef=\"let row\"> {{row.medida}} </td>\n                                    </ng-container>\n\n                                    <ng-container matColumnDef=\"totalItems\">\n                                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Quantidade </th>\n                                        <td mat-cell *matCellDef=\"let row\"> {{row.totalItems}} </td>\n                                    </ng-container>\n\n                                    <tr mat-header-row *matHeaderRowDef=\"displayedColumnsItems;sticky: true\"></tr>\n                                    <tr mat-row *matRowDef=\"let row; columns: displayedColumnsItems;\"></tr>\n\n                                    <tr class=\"mat-row\" *matNoDataRow>\n                                        <td class=\"mat-cell\" colspan=\"4\">Nenhum dado corresponde ao filtro\n                                            \"{{input.value}}\"</td>\n                                    </tr>\n                                </table>\n                            </div>\n\n                        </div>\n\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"stats\">\n                            <i class=\"material-icons\">access_time</i> <b>Atualizado em {{timeDate | date:\n                                \"HH:mm:ss\"}}</b>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n    </div>\n</div>");

/***/ }),

/***/ "IGO/":
/*!**************************************************************!*\
  !*** ./src/app/shared/services/cadastro/cadastro.service.ts ***!
  \**************************************************************/
/*! exports provided: CadastroService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastroService", function() { return CadastroService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! environments/environment */ "AytR");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CadastroService = /** @class */ (function () {
    function CadastroService(httpClient) {
        this.httpClient = httpClient;
        this.apiIbge = environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiIbge.databaseURL;
        this.apiUrl = environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].dataBaseConfig.databaseURL;
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]({ 'Content-Type': 'application/json' })
        };
    }
    CadastroService.prototype.getMunicipios = function () {
        return this.httpClient.get(this.apiIbge);
    };
    CadastroService.prototype.getOrders = function (page) {
        if (page === void 0) { page = 0; }
        return this.httpClient.get(this.apiUrl + "orders", { params: { page: page + '' } })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(2), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    CadastroService.prototype.getOrderById = function (id) {
        return this.httpClient.get(this.apiUrl + "orders/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(2), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    CadastroService.prototype.saveOrder = function (cad) {
        console.log(cad);
        return this.httpClient.post(this.apiUrl + "orders", JSON.stringify(cad), this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(2), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    CadastroService.prototype.updateOrder = function (id, cad) {
        return this.httpClient.put(this.apiUrl + "orders/" + id.id, JSON.stringify(cad), this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    CadastroService.prototype.deleteOrder = function (cad) {
        return this.httpClient.delete(this.apiUrl + "orders/" + cad.id, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    CadastroService.prototype.handleError = function (error) {
        var errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        }
        else {
            errorMessage = "C\u00F3digo do erro: " + error.status + ", " + ("menssagem: " + error.message);
        }
        console.log(errorMessage);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(errorMessage);
    };
    ;
    CadastroService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] }
    ]; };
    CadastroService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]])
    ], CadastroService);
    return CadastroService;
}());



/***/ }),

/***/ "IqXj":
/*!*************************************************************!*\
  !*** ./src/app/layouts/admin-layout/admin-layout.module.ts ***!
  \*************************************************************/
/*! exports provided: AdminLayoutModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminLayoutModule", function() { return AdminLayoutModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _admin_layout_routing__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./admin-layout.routing */ "qZ7x");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../dashboard/dashboard.component */ "QX6l");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/button */ "bTqV");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/input */ "qFsG");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/core */ "FKr1");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/form-field */ "kmnG");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/tooltip */ "Qu3c");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/select */ "d3UM");
/* harmony import */ var app_cadastro_cadastro_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! app/cadastro/cadastro.component */ "gK0J");
/* harmony import */ var app_material_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! app/material-module */ "j5wd");
/* harmony import */ var app_login_login_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! app/login/login.component */ "vtpD");
/* harmony import */ var app_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! app/not-found/not-found.component */ "nod/");
/* harmony import */ var app_shared_services_login_login_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! app/shared/services/login/login.service */ "EVec");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var AdminLayoutModule = /** @class */ (function () {
    function AdminLayoutModule() {
    }
    AdminLayoutModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_admin_layout_routing__WEBPACK_IMPORTED_MODULE_4__["AdminLayoutRoutes"]),
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_6__["MatButtonModule"],
                _angular_material_core__WEBPACK_IMPORTED_MODULE_8__["MatRippleModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__["MatFormFieldModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_7__["MatInputModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_11__["MatSelectModule"],
                _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_10__["MatTooltipModule"],
                app_material_module__WEBPACK_IMPORTED_MODULE_13__["DemoMaterialModule"],
            ],
            declarations: [
                _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"],
                app_cadastro_cadastro_component__WEBPACK_IMPORTED_MODULE_12__["CadastroComponent"],
                app_login_login_component__WEBPACK_IMPORTED_MODULE_14__["LoginComponent"],
                app_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_15__["NotFoundComponent"],
            ],
            providers: [app_shared_services_login_login_service__WEBPACK_IMPORTED_MODULE_16__["LoginService"]],
        })
    ], AdminLayoutModule);
    return AdminLayoutModule;
}());



/***/ }),

/***/ "QX6l":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _raw_loader_dashboard_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./dashboard.component.html */ "H/d9");
/* harmony import */ var _dashboard_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dashboard.component.scss */ "7igb");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/table */ "+0xr");
/* harmony import */ var app_shared_services_dashboard_dashboard_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/services/dashboard/dashboard.service */ "0cW3");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! chart.js */ "MO+k");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(dashboardService, toastr) {
        this.dashboardService = dashboardService;
        this.toastr = toastr;
        this.displayedColumns = ['name', 'local', 'medida', 'totalItems'];
        this.displayedColumnsLocal = ['name', 'municipio', 'medida', 'totalItems'];
        this.displayedColumnsItems = ['local', 'municipio', 'medida', 'totalItems'];
        this.timeDate = new Date();
        this.isLoading = true;
        this.isLoadingItems = true;
        this.isMessageSelectedMunicipio = true;
        this.isLoadingLocais = true;
        this.isLoadingItem = true;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.getItems();
        this.getLabelsLocal();
        this.getLabelsItem();
    };
    // compareObjects(municipio: any): boolean {
    //   console.log( municipio)
    //   return municipio;
    // }
    DashboardComponent.prototype.applyFilter = function (event) {
        var filterValue = event.target.value;
        this.dataItemsTable.filter = filterValue.trim().toLowerCase();
    };
    DashboardComponent.prototype.applyFilterLocal = function (event) {
        var filterValue = event.target.value;
        this.dataItemsTableLocais.filter = filterValue.trim().toLowerCase();
    };
    DashboardComponent.prototype.applyFilterItems = function (event) {
        var filterValue = event.target.value;
        this.dataItemsTableItems.filter = filterValue.trim().toLowerCase();
    };
    DashboardComponent.prototype.getItems = function () {
        var _this = this;
        this.dashboardService.getItems().subscribe(function (data) {
            _this.data = data;
            _this.isLoading = false;
            _this.createChartGeral();
        }, function (error) {
            _this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
        });
    };
    DashboardComponent.prototype.getLabelsLocal = function () {
        var _this = this;
        this.dashboardService.getAllLocal().subscribe(function (data) {
            _this.labelsLocais = data;
            _this.filteredListLocal = data;
        }, function (error) {
            _this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
        });
    };
    DashboardComponent.prototype.getLabelsItem = function () {
        var _this = this;
        this.dashboardService.getAllItem().subscribe(function (data) {
            _this.labelsItems = data;
            _this.filteredListItems = data;
        }, function (error) {
            _this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
        });
    };
    DashboardComponent.prototype.getItemsMunicipio = function (municipio) {
        var _this = this;
        this.isLoadingItems = true;
        this.selectedMunicipio = municipio;
        this.dashboardService.getItemsMunicipio(municipio).subscribe(function (data) {
            _this.isLoadingItems = false;
            _this.dataItemsMunicipio = data;
            _this.dataItemsTable = new _angular_material_table__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](data);
            _this.isMessageSelectedMunicipio = false;
            // this.createChartItems();
        }, function (error) {
            _this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
        });
    };
    DashboardComponent.prototype.getItemsByLocal = function (local) {
        var _this = this;
        this.isLoadingLocais = true;
        this.selectedLocal = local;
        this.dashboardService.getByLocal(local).subscribe(function (data) {
            _this.isLoadingLocais = false;
            _this.dataItemsTableLocais = new _angular_material_table__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](data);
        }, function (error) {
            _this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
        });
    };
    DashboardComponent.prototype.getByLocalItems = function (item) {
        var _this = this;
        this.isLoadingItem = true;
        this.selectedItem = item;
        this.dashboardService.getByItem(item).subscribe(function (data) {
            _this.isLoadingItem = false;
            _this.dataItemsTableItems = new _angular_material_table__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](data);
        }, function (error) {
            _this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
        });
    };
    DashboardComponent.prototype.getChartLabelsGeral = function () {
        var labels = [];
        var municipio = this.data.municipio;
        municipio.forEach(function (data, i) {
            labels.push(municipio[i].municipio);
        });
        this.labelsMunicipios = labels;
        this.filteredListMunicipios = labels;
        return labels;
    };
    DashboardComponent.prototype.getDatasetsGeral = function () {
        this.timeDate = new Date();
        var datasets = {};
        var dataData = [];
        var municipio = this.data.municipio;
        municipio.forEach(function (data, i) {
            dataData.push(municipio[i].totalItemsDoados);
        });
        datasets = ({
            backgroundColor: '#55CBCD',
            borderColor: '#555',
            barThickness: 25,
            // categoryPercentage: 1.0,
            // barPercentage: 0.5,
            data: dataData,
            borderWidth: 1
        });
        return datasets;
    };
    DashboardComponent.prototype.createChartGeral = function () {
        this.canvasGeral = this.mychartGeral.nativeElement;
        this.ctxGeral = this.canvasGeral.getContext('2d');
        var myChart = new chart_js__WEBPACK_IMPORTED_MODULE_5__["Chart"](this.ctxGeral, {
            type: 'bar',
            data: {
                labels: this.getChartLabelsGeral(),
                datasets: [this.getDatasetsGeral()],
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Total de items doados por municípios',
                    fontSize: 25
                },
                scales: {
                    xAxes: [{
                            ticks: {
                                fontSize: 17
                            }
                        }],
                    yAxes: [{
                            ticks: {
                                fontSize: 20,
                                beginAtZero: true
                            },
                            scaleLabel: {
                                labelString: 'Quantidade de items',
                                display: true,
                                fontSize: 22
                            }
                        }],
                },
                legend: {
                    display: false,
                },
                plugins: {
                    legend: {
                        labels: {
                            fontSize: 20
                        }
                    }
                },
                animation: {
                    onComplete: function () {
                        if (!this.rectangleSet) {
                            var scale = window.devicePixelRatio;
                            var sourceCanvas = this.chart.canvas;
                            var copyWidth = this.chart.chart.scales['y-axis-0'].width - 10;
                            var copyHeight = this.chart.chart.scales['y-axis-0'].height + this.scales['y-axis-0'].top + 10;
                            var can = document.getElementById('chartAxisGeral');
                            if (can != null) {
                                var targetCtx = can.getContext('2d');
                                targetCtx.scale(scale, scale);
                                targetCtx.canvas.width = copyWidth * scale;
                                targetCtx.canvas.height = copyHeight * scale;
                                targetCtx.canvas.style.width = copyWidth + "px";
                                targetCtx.canvas.style.height = copyHeight + "px";
                                targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth * scale, copyHeight * scale, 0, 0, copyWidth * scale, copyHeight * scale);
                                var sourceCtx = sourceCanvas.getContext('2d');
                                // Normalize coordinate system to use css pixels.
                                sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
                                this.rectangleSet = true;
                            }
                            else {
                                // report the error.  
                                console.log("aqui na rolou ");
                            }
                        }
                    },
                    onProgress: function () {
                        if (this.rectangleSet === true) {
                            var copyWidth = this.chart.chart.scales['y-axis-0'].width;
                            var copyHeight = this.chart.chart.scales['y-axis-0'].height +
                                this.chart.chart.scales['y-axis-0'].top +
                                10;
                            var sourceCtx = this.chart.canvas.getContext('2d');
                            sourceCtx.clearRect(0, 0, copyWidth, copyHeight);
                        }
                    },
                }
            }
        });
    };
    DashboardComponent.prototype.getChartLabelsItems = function () {
        var _this = this;
        var labels = [];
        this.dataItemsMunicipio.forEach(function (data, i) {
            labels.push(_this.dataItemsMunicipio[i].name);
        });
        return labels;
    };
    DashboardComponent.prototype.getDatasetsItems = function () {
        var _this = this;
        this.timeDate = new Date();
        var datasets = {};
        var dataData = [];
        this.dataItemsMunicipio.forEach(function (data, i) {
            dataData.push(_this.dataItemsMunicipio[i].totalItems);
        });
        datasets = ({
            backgroundColor: '#55CBCD',
            borderColor: '#555',
            barThickness: 10,
            // categoryPercentage: 1.0,
            // barPercentage: 0.5,
            data: dataData,
            borderWidth: 1
        });
        return datasets;
    };
    DashboardComponent.prototype.createChartItems = function () {
        var _this = this;
        if (this.myChartItems != null) {
            this.myChartItems.destroy();
        }
        this.canvasItems = this.mychartItems.nativeElement;
        this.ctxItems = this.canvasItems.getContext('2d');
        this.myChartItems = new chart_js__WEBPACK_IMPORTED_MODULE_5__["Chart"](this.ctxItems, {
            type: 'bar',
            data: {
                labels: this.getChartLabelsItems(),
                datasets: [this.getDatasetsItems()],
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Total Tipos de Items',
                    fontSize: 25
                },
                scales: {
                    xAxes: [{
                            ticks: {
                                fontSize: 10
                            }
                        }],
                    yAxes: [{
                            ticks: {
                                fontSize: 20,
                                beginAtZero: true
                            },
                            scaleLabel: {
                                labelString: 'Quantidade de items',
                                display: true,
                                fontSize: 22
                            }
                        }],
                },
                legend: {
                    display: false,
                },
                plugins: {
                    legend: {
                        labels: {
                            fontSize: 20
                        }
                    }
                },
                animation: {
                    onComplete: function () {
                        if (!this.rectangleSet) {
                            var scale = window.devicePixelRatio;
                            var sourceCanvas = this.chart.canvas;
                            var copyWidth = this.chart.chart.scales['y-axis-0'].width - 10;
                            var copyHeight = this.chart.chart.scales['y-axis-0'].height + this.scales['y-axis-0'].top + 10;
                            var can = document.getElementById('chartAxisItems');
                            var targetCtx = can.getContext('2d');
                            targetCtx.scale(scale, scale);
                            targetCtx.canvas.width = copyWidth * scale;
                            targetCtx.canvas.height = copyHeight * scale;
                            targetCtx.canvas.style.width = copyWidth + "px";
                            targetCtx.canvas.style.height = copyHeight + "px";
                            targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth * scale, copyHeight * scale, 0, 0, copyWidth * scale, copyHeight * scale);
                            var sourceCtx = sourceCanvas.getContext('2d');
                            // Normalize coordinate system to use css pixels.
                            sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
                            this.rectangleSet = true;
                        }
                    },
                    onProgress: function () {
                        if (this.rectangleSet === true) {
                            var copyWidth = this.chart.chart.scales['y-axis-0'].width;
                            var copyHeight = this.chart.chart.scales['y-axis-0'].height +
                                this.chart.chart.scales['y-axis-0'].top +
                                10;
                            var sourceCtx = this.chart.canvas.getContext('2d');
                            sourceCtx.clearRect(0, 0, copyWidth, copyHeight);
                        }
                    },
                }
            }
        });
        setTimeout(function () {
            _this.isLoadingItems = false;
        }, 1000);
    };
    DashboardComponent.ctorParameters = function () { return [
        { type: app_shared_services_dashboard_dashboard_service__WEBPACK_IMPORTED_MODULE_4__["DashboardService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"] }
    ]; };
    DashboardComponent.propDecorators = {
        mychartGeral: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"], args: ['myChartGeral',] }],
        mychartItems: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"], args: ['myChartItems',] }]
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-dashboard',
            template: _raw_loader_dashboard_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_dashboard_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [app_shared_services_dashboard_dashboard_service__WEBPACK_IMPORTED_MODULE_4__["DashboardService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "VziJ":
/*!***************************************************!*\
  !*** ./src/app/not-found/not-found.component.css ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25vdC1mb3VuZC9ub3QtZm91bmQuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "ZTFi":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  display: block;\n}\n\n.login-page {\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: #eeeeee;\n  text-align: center;\n  color: #3C4858;\n}\n\n.login-page .col-lg-4 {\n  padding: 0;\n}\n\n.login-page .input-lg {\n  height: 46px;\n  padding: 10px 16px;\n  font-size: 18px;\n  line-height: 1.3333333;\n  border-radius: 0;\n}\n\n.login-page .input-underline {\n  background: 0 0;\n  border: none;\n  box-shadow: none;\n  border-bottom: 2px solid #3C4858;\n  color: #3C4858;\n  border-radius: 0;\n}\n\n.login-page .input-underline:focus {\n  border-bottom: 2px solid #3C4858;\n  box-shadow: none;\n}\n\n.login-page .rounded-btn {\n  border-radius: 50px;\n  color: #3C4858;\n  background: #eeeeee;\n  border: 2px solid #3C4858;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 25px;\n}\n\n.login-page .rounded-btn:hover,\n.login-page .rounded-btn:focus,\n.login-page .rounded-btn:active,\n.login-page .rounded-btn:visited {\n  color: #3C4858;\n  border: 2px solid #3C4858;\n  outline: none;\n}\n\n.login-page h1 {\n  font-weight: 300;\n  margin-top: 20px;\n  margin-bottom: 10px;\n  font-size: 36px;\n}\n\n.login-page h1 small {\n  color: #3C4858;\n}\n\n.login-page .form-group {\n  padding: 8px 0;\n}\n\n.login-page .form-group input::-webkit-input-placeholder {\n  color: #3C4858 !important;\n}\n\n.login-page .form-group input:-moz-placeholder {\n  /* Firefox 18- */\n  color: #3C4858 !important;\n}\n\n.login-page .form-group input::-moz-placeholder {\n  /* Firefox 19+ */\n  color: #3C4858 !important;\n}\n\n.login-page .form-group input:-ms-input-placeholder {\n  color: #3C4858 !important;\n}\n\n.login-page .form-content {\n  padding: 40px 0;\n}\n\n.login-page .user-avatar {\n  border: 0px solid #3C4858;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxjQUFBO0FBQUo7O0FBRUE7RUFFSSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBRUEsbUJBWHFCO0VBWXJCLGtCQUFBO0VBQ0EsY0FBQTtBQURKOztBQUdJO0VBQ0ksVUFBQTtBQURSOztBQUdJO0VBQ0ksWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7QUFEUjs7QUFHSTtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQ0FBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQURSOztBQUdJO0VBQ0ksZ0NBQUE7RUFDQSxnQkFBQTtBQURSOztBQUdJO0VBRUksbUJBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBekNpQjtFQTBDakIseUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FBRFI7O0FBR0k7Ozs7RUFJSSxjQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0FBRFI7O0FBSUk7RUFDSSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FBRlI7O0FBR1E7RUFDSSxjQUFBO0FBRFo7O0FBS0k7RUFDSSxjQUFBO0FBSFI7O0FBSVE7RUFDSSx5QkFBQTtBQUZaOztBQUtRO0VBQ0ksZ0JBQUE7RUFDQSx5QkFBQTtBQUhaOztBQU1RO0VBQ0ksZ0JBQUE7RUFDQSx5QkFBQTtBQUpaOztBQU9RO0VBQ0kseUJBQUE7QUFMWjs7QUFRSTtFQUNJLGVBQUE7QUFOUjs7QUFRSTtFQUdJLHlCQUFBO0FBUlIiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjojZWVlZWVlO1xuOmhvc3Qge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xufVxuLmxvZ2luLXBhZ2Uge1xuICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICBib3R0b206IDA7XG4gICAgLy8gb3ZlcmZsb3c6IGF1dG87XG4gICAgYmFja2dyb3VuZDogJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBjb2xvcjogIzNDNDg1ODtcbiAgICAvLyBwYWRkaW5nOiAzZW07XG4gICAgLmNvbC1sZy00IHtcbiAgICAgICAgcGFkZGluZzogMDtcbiAgICB9XG4gICAgLmlucHV0LWxnIHtcbiAgICAgICAgaGVpZ2h0OiA0NnB4O1xuICAgICAgICBwYWRkaW5nOiAxMHB4IDE2cHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDEuMzMzMzMzMztcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcbiAgICB9XG4gICAgLmlucHV0LXVuZGVybGluZSB7XG4gICAgICAgIGJhY2tncm91bmQ6IDAgMDtcbiAgICAgICAgYm9yZGVyOiBub25lO1xuICAgICAgICBib3gtc2hhZG93OiBub25lO1xuICAgICAgICBib3JkZXItYm90dG9tOiAycHggc29saWQgIzNDNDg1ODtcbiAgICAgICAgY29sb3I6IzNDNDg1ODtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcbiAgICB9XG4gICAgLmlucHV0LXVuZGVybGluZTpmb2N1cyB7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjM0M0ODU4O1xuICAgICAgICBib3gtc2hhZG93OiBub25lO1xuICAgIH1cbiAgICAucm91bmRlZC1idG4ge1xuICAgICAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDUwcHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gICAgICAgIGNvbG9yOiMzQzQ4NTg7XG4gICAgICAgIGJhY2tncm91bmQ6ICR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjtcbiAgICAgICAgYm9yZGVyOiAycHggc29saWQgIzNDNDg1ODtcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICBsaW5lLWhlaWdodDogNDBweDtcbiAgICAgICAgcGFkZGluZzogMCAyNXB4O1xuICAgIH1cbiAgICAucm91bmRlZC1idG46aG92ZXIsXG4gICAgLnJvdW5kZWQtYnRuOmZvY3VzLFxuICAgIC5yb3VuZGVkLWJ0bjphY3RpdmUsXG4gICAgLnJvdW5kZWQtYnRuOnZpc2l0ZWQge1xuICAgICAgICBjb2xvcjogIzNDNDg1ODtcbiAgICAgICAgYm9yZGVyOiAycHggc29saWQgIzNDNDg1ODtcbiAgICAgICAgb3V0bGluZTogbm9uZTtcbiAgICB9XG5cbiAgICBoMSB7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMzZweDtcbiAgICAgICAgc21hbGwge1xuICAgICAgICAgICAgY29sb3I6ICMzQzQ4NTg7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuZm9ybS1ncm91cCB7XG4gICAgICAgIHBhZGRpbmc6IDhweCAwO1xuICAgICAgICBpbnB1dDo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlciB7XG4gICAgICAgICAgICBjb2xvcjojM0M0ODU4ICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cblxuICAgICAgICBpbnB1dDotbW96LXBsYWNlaG9sZGVyIHtcbiAgICAgICAgICAgIC8qIEZpcmVmb3ggMTgtICovXG4gICAgICAgICAgICBjb2xvcjogIzNDNDg1OCFpbXBvcnRhbnQ7XG4gICAgICAgIH1cblxuICAgICAgICBpbnB1dDo6LW1vei1wbGFjZWhvbGRlciB7XG4gICAgICAgICAgICAvKiBGaXJlZm94IDE5KyAqL1xuICAgICAgICAgICAgY29sb3I6IzNDNDg1OCFpbXBvcnRhbnQ7XG4gICAgICAgIH1cblxuICAgICAgICBpbnB1dDotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xuICAgICAgICAgICAgY29sb3I6IzNDNDg1OCAhaW1wb3J0YW50O1xuICAgICAgICB9XG4gICAgfVxuICAgIC5mb3JtLWNvbnRlbnQge1xuICAgICAgICBwYWRkaW5nOiA0MHB4IDA7XG4gICAgfVxuICAgIC51c2VyLWF2YXRhciB7XG4gICAgICAgIC8vIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAvLyBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgIGJvcmRlcjogMHB4IHNvbGlkIzNDNDg1ODtcbiAgICB9XG59XG4iXX0= */");

/***/ }),

/***/ "gK0J":
/*!************************************************!*\
  !*** ./src/app/cadastro/cadastro.component.ts ***!
  \************************************************/
/*! exports provided: CadastroComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastroComponent", function() { return CadastroComponent; });
/* harmony import */ var _raw_loader_cadastro_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./cadastro.component.html */ "/dgT");
/* harmony import */ var _cadastro_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./cadastro.component.scss */ "rCWb");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/paginator */ "M9IT");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/sort */ "Dh3D");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/table */ "+0xr");
/* harmony import */ var app_shared_services_cadastro_cadastro_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/shared/services/cadastro/cadastro.service */ "IGO/");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var CadastroComponent = /** @class */ (function () {
    function CadastroComponent(dialog, fb, cadastroService, toastr) {
        this.dialog = dialog;
        this.fb = fb;
        this.cadastroService = cadastroService;
        this.toastr = toastr;
        this.displayedColumns = ['termo', 'municipio', 'local', 'data', 'opcao'];
        this.isLoading = true;
        this.controlDelete = false;
        this.controlSubmit = false;
        this.controlPreviewFormat = false;
        this.municipios = [];
        this.filterInputRow = {};
    }
    CadastroComponent.prototype.ngOnInit = function () {
        this.getUnidadeMedida();
        this.getItemsSelected();
        this.getMunicipios();
        this.getCadastros();
        this.userform = this.fb.group({
            'termo': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            'municipio': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            'data': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            'items': this.fb.array([]),
            'local': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            'evidencia': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            'observacao': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
        });
    };
    CadastroComponent.prototype.items = function () {
        return this.userform.get("items");
    };
    CadastroComponent.prototype.newItem = function () {
        return this.fb.group({
            'name': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            'quantidade': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            'medida': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required)
        });
    };
    CadastroComponent.prototype.getItem = function () {
        return this.fb.group({
            'name': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.dataFormItem.items, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            'quantidade': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.dataFormItem.quantidades, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            'medida': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.dataFormItem.medidas, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required)
        });
    };
    CadastroComponent.prototype.handleClickSelect = function (i) {
        this.filterInputRow[i] = this.filteredListItems;
    };
    CadastroComponent.prototype.inputFilterResert = function (event, i) {
        this.filterInputRow[i] = event;
    };
    CadastroComponent.prototype.addItem = function () {
        this.items().push(this.newItem());
    };
    CadastroComponent.prototype.editItem = function () {
        this.items().push(this.getItem());
    };
    CadastroComponent.prototype.removeItem = function (i) {
        this.items().removeAt(i);
    };
    CadastroComponent.prototype.onSubmit = function () {
        this.submitted = true;
        if (this.userform.invalid) {
            return;
        }
        if (this.controlSubmit == true) {
            this.postCadastro(this.userform.value);
            this.getCadastros();
            this.dialog.closeAll();
        }
        else if (this.controlSubmit == false) {
            this.updateCadastro(this.userform.value);
            this.getCadastros();
            this.dialog.closeAll();
        }
    };
    CadastroComponent.prototype.readFile = function (file) {
        var reader = new FileReader();
        var loadend = Object(rxjs__WEBPACK_IMPORTED_MODULE_10__["fromEvent"])(reader, 'loadend').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (read) {
            return read.target.result;
        }));
        reader.readAsDataURL(file);
        return loadend;
    };
    CadastroComponent.prototype.onFileChange = function (fileList) {
        var _this = this;
        this.readFile(fileList[0]).subscribe(function (res) {
            _this.controlPreviewFormat = fileList[0].type == 'application/pdf' ? true : false;
            _this.imgSrc = res;
            _this.userform.controls['evidencia'].setValue(res);
        });
    };
    CadastroComponent.prototype.getMunicipios = function () {
        var _this = this;
        this.cadastroService.getMunicipios().subscribe(function (municipios) {
            _this.municipios = municipios;
            _this.filteredListMunicipios = municipios;
        });
    };
    CadastroComponent.prototype.getUnidadeMedida = function () {
        this.unidadeDeMedida = [
            { value: "UNIDADE" },
            { value: "CAIXA" },
            { value: "PACOTE" },
        ];
    };
    CadastroComponent.prototype.getItemsSelected = function () {
        this.filteredListItems = [
            {
                nome: "EPI's",
                items: [
                    { value: 'LUVA (CIRÚRGICA - PROCEDIMENTO - DESCARTAVEL)', viewValue: 'LUVA (CIRÚRGICA - PROCEDIMENTO - DESCARTAVEL)' },
                    { value: 'MÁSCARA HOSPITALAR', viewValue: 'MÁSCARA HOSPITALAR' },
                    { value: 'TOUCA', viewValue: 'TOUCA' },
                    { value: 'AVENTAL', viewValue: 'AVENTAL' },
                    { value: 'PROTETOR FACIAL - VISEIRA', viewValue: 'PROTETOR FACIAL - VISEIRA' },
                    { value: 'PROPÉ', viewValue: 'PROPÉ' },
                    { value: 'MACACÃO', viewValue: 'MACACÃO' },
                    { value: 'ÓCULOS DE PROTEÇÃO', viewValue: 'ÓCULOS DE PROTEÇÃO' },
                ]
            },
            {
                nome: 'MEDICAMENTO',
                items: [
                    { value: 'ACEBROFILINA', viewValue: 'ACEBROFILINA' },
                    { value: 'ACETILCISTEÍNA (XAROPE - COMPRIMIDO - SACHÊ - AMPOLA)', viewValue: 'ACETILCISTEÍNA (XAROPE - COMPRIMIDO - SACHÊ - AMPOLA)' },
                    { value: 'AMBROSOL', viewValue: 'AMBROSOL' },
                    { value: 'AZITROMICINA', viewValue: 'AZITROMICINA' },
                    { value: 'CAPTOPRIL', viewValue: 'CAPTOPRIL' },
                    { value: 'CARVEDILOL - CARVEDIOL - CARDBET', viewValue: 'CARVEDILOL - CARVEDIOL - CARDBET' },
                    { value: 'CEFTRIAXONA', viewValue: 'CEFTRIAXONA' },
                    { value: 'CISTEIL', viewValue: 'CISTEIL' },
                    { value: 'CLARITROMICINA INJETÁVEL', viewValue: 'CLARITROMICINA INJETÁVEL' },
                    { value: 'COMPLEXO B INJETÁVEL', viewValue: 'COMPLEXO B INJETÁVEL' },
                    { value: 'DESAMETASONA - DEXASON', viewValue: 'DESAMETASONA - DEXASON' },
                    { value: 'DIPIRONA (GOTAS - COMPRIMIDO - ENDOVENOSO - XAPORE)', viewValue: 'DIPIRONA (GOTAS - COMPRIMIDO - ENDOVENOSO - XAPORE)' },
                    { value: 'ENOXAPARINA', viewValue: 'ENOXAPARINA' },
                    { value: 'FENTANILA', viewValue: 'FENTANILA' },
                    { value: 'FLUCANIL', viewValue: 'FLUCANIL' },
                    { value: 'IVERMECTINA', viewValue: 'IVERMECTINA' },
                    { value: 'MAXAGILNA', viewValue: 'MAXAGILNA' },
                    { value: 'LOSARTANA', viewValue: 'LOSARTANA' },
                    { value: 'OMEPRAZOL - EUPEPT - TEUTOZOL', viewValue: 'OMEPRAZOL - EUPEPT - TEUTOZOL' },
                    { value: 'PARECETAMOL (GOTAS - COMPRIMIDO - ENDOVENOSO)', viewValue: 'PARECETAMOL (GOTAS - COMPRIMIDO - ENDOVENOSO)' },
                    { value: 'PIPERACILINA + TAZOBACTAM', viewValue: 'PIPERACILINA + TAZOBACTAM' },
                    { value: 'PREDNIZONA', viewValue: 'PREDNIZONA' },
                    { value: 'SORO', viewValue: 'SORO' },
                    { value: 'VITA A-Z - VITAMINA A-Z', viewValue: 'VITA A-Z - VITAMINA A-Z' },
                    { value: 'VITAMINA C', viewValue: 'VITAMINA C' },
                    { value: 'VITAMINA D', viewValue: 'VITAMINA D' },
                    { value: 'TILEMAX', viewValue: 'TILEMAX' }
                ]
            },
            {
                nome: 'OXIGÊNIO',
                items: [
                    { value: 'CILINDRO', viewValue: 'CILINDRO' },
                ]
            },
            {
                nome: 'EQUIPAMENTO',
                items: [
                    { value: 'AMBU', viewValue: 'AMBU' },
                    { value: 'BIPAP', viewValue: 'BIPAP' },
                    { value: 'CPAP', viewValue: 'CPAP' },
                    { value: 'CONCENTRADOR DE OXIGÊNIO', viewValue: 'CONCENTRADOR DE OXIGÊNIO' },
                    { value: 'KIT VÁLVULA RESPIRADORA DOBRAVEL', viewValue: 'KIT VÁLVULA RESPIRADORA DOBRAVEL' },
                    { value: 'KIT VÁLVULA COM FLUXOMETRO', viewValue: 'KIT VÁLVULA COM FLUXOMETRO' },
                    { value: 'CATETER NASAL', viewValue: 'CATETER NASAL' },
                    { value: 'LÂMINA LARINGO', viewValue: 'LÂMINA LARINGO' },
                    { value: 'MANÔMETRO', viewValue: 'MANÔMETRO' },
                    { value: 'REANIMADOR MANUAL', viewValue: 'REANIMADOR MANUAL' },
                    { value: 'REGULADOR DE PRESSÃO', viewValue: 'REGULADOR DE PRESSÃO' },
                    { value: 'KIT RESPIRADOR COM PEDESTAL PARA MONITOR (CABO DE ECG - MAGUEIRA EXTENSORA - BRAÇADEIRA - SENSOR DE OXIMETRIA - SENSOR DE TEMPERATURA)', viewValue: 'KIT RESPIRADOR COM PEDESTAL PARA MONITOR (CABO DE ECG - MAGUEIRA EXTENSORA - BRAÇADEIRA - SENSOR DE OXIMETRIA - SENSOR DE TEMPERATURA)' },
                    { value: 'MONITOR MULTIPARAMÉTRICO', viewValue: 'MONITOR MULTIPARAMÉTRICO' },
                    { value: 'REANIMADOR MANUAL', viewValue: 'REANIMADOR MANUAL' },
                    { value: 'CONEXÃO Y', viewValue: 'CONEXÃO Y' },
                    { value: 'MASCARA NÃO REINALAÇÃO', viewValue: 'MASCARA NÃO REINALAÇÃO' },
                    { value: 'REANIMADOR MANUAL', viewValue: 'REANIMADOR MANUAL' },
                    { value: 'FLUXOMETRO', viewValue: 'FLUXOMETRO' },
                    { value: 'UMIDIFICADOR', viewValue: 'UMIDIFICADOR' },
                    { value: 'REGULADOR DE PRESSÃO', viewValue: 'REGULADOR DE PRESSÃO' },
                    { value: 'VÁLVULA DE EXALAÇÃO', viewValue: 'VÁLVULA DE EXALAÇÃO' },
                    { value: 'VENTILADOR MECÂNICO', viewValue: 'VENTILADOR MECÂNICO' },
                    { value: 'CIRCUITO DUPLO', viewValue: 'CIRCUITO DUPLO' },
                ]
            },
            {
                nome: 'MATERIAL/INSUMO',
                items: [
                    { value: 'ÁGUA SANITÁRIA', viewValue: 'ÁGUA SANITÁRIA' },
                    { value: 'AGULHA', viewValue: 'AGULHA' },
                    { value: 'ALCOOL', viewValue: 'ALCOOL' },
                    { value: 'KIT SANIZANTE (DESINFETANTE HOSPITALAR + PULVERIZADOR)', viewValue: 'KIT SANIZANTE (DESINFETANTE HOSPITALAR + PULVERIZADOR)' },
                    { value: 'APARELHO DE PRESSÃO', viewValue: 'APARELHO DE PRESSÃO' },
                    { value: 'ATADURA', viewValue: 'ATADURA' },
                    { value: 'CADEIRA DE BANHO', viewValue: 'CADEIRA DE BANHO' },
                    { value: 'CADEIRA DE RODAS', viewValue: 'CADEIRA DE RODAS' },
                    { value: 'DESINFETANTE', viewValue: 'DESINFETANTE' },
                    { value: 'DETERGENTE', viewValue: 'DETERGENTE' },
                    { value: 'ESCADA PARA MACA', viewValue: 'ESCADA PARA MACA' },
                    { value: 'ESPONJA DE LIMPEZA', viewValue: 'ESPONJA DE LIMPEZA' },
                    { value: 'ESPUMA HIGIENIZADORA', viewValue: 'ESPUMA HIGIENIZADORA' },
                    { value: 'FRALDA', viewValue: 'FRALDA' },
                    { value: 'KIT NEBULIZAÇÃO', viewValue: 'KIT NEBULIZAÇÃO' },
                    { value: 'KIT LARINGOSCÓPIO', viewValue: 'KIT LARINGOSCÓPIO' },
                    { value: 'LENÇOL HOSPITALAR DE PAPEL', viewValue: 'LENÇOL HOSPITALAR DE PAPEL' },
                    { value: 'MÁSCARA DE TECIDO', viewValue: 'MÁSCARA DE TECIDO' },
                    { value: 'OXÍMETRO', viewValue: 'OXÍMETRO' },
                    { value: 'PILHA', viewValue: 'PILHA' },
                    { value: 'ALSERINGACOOL', viewValue: 'SERINGA' },
                    { value: 'SONDA', viewValue: 'SONDA' },
                    { value: 'SUPORTE PARA SORO', viewValue: 'SUPORTE PARA SORO' },
                    { value: 'TERMOMETRO - SENSOR DE TEMPERATURA', viewValue: 'TERMOMETRO - SENSOR DE TEMPERATURA' },
                    { value: 'TUBO ENDOTRAQUIAL', viewValue: 'TUBO ENDOTRAQUIAL' },
                    { value: 'UMIDIFICADOR', viewValue: 'UMIDIFICADOR' },
                    { value: 'VÁLVULA DE CILINDRO', viewValue: 'VÁLVULA DE CILINDRO' }
                ]
            },
            {
                nome: 'ALIMENTOS',
                items: [
                    { value: 'ÁGUA MINERAL', viewValue: 'ÁGUA MINERAL' },
                    { value: 'CESTA BÁSICA', viewValue: 'CESTA BÁSICA' },
                    { value: 'FARDO DE MILITOS', viewValue: 'FARDO DE MILITOS' },
                ]
            }
        ];
    };
    CadastroComponent.prototype.getCadastros = function () {
        var _this = this;
        this.cadastroService.getOrders().subscribe(function (data) {
            var cadastros = data.cadastros, totalPages = data.totalPages;
            _this.totalPages = totalPages;
            _this.cadastros = new _angular_material_table__WEBPACK_IMPORTED_MODULE_7__["MatTableDataSource"](cadastros);
            // this.paginator.pageIndex = 0;
            // this.cadastros.paginator = this.paginator;
            // console.log(this.paginator)
            _this.cadastros.sort = _this.sort;
            _this.data = data;
            _this.isLoading = false;
        }, function (error) {
            _this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
        });
    };
    CadastroComponent.prototype.postCadastro = function (cadastro) {
        var _this = this;
        this.cadastroService.saveOrder(cadastro).subscribe(function () {
            _this.toastr.success('Cadastrado!', 'Sucesso!');
            _this.isLoading = true;
            _this.getCadastros();
        }, function (err) {
            _this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
        });
    };
    CadastroComponent.prototype.deleteCadastro = function () {
        var _this = this;
        this.cadastroService.deleteOrder(this.cadastroKey)
            .subscribe(function () {
            _this.isLoading = true;
            _this.getCadastros();
            _this.dialog.closeAll();
            _this.toastr.success('Deletado!', 'Sucesso!');
        }, function (err) {
            _this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
        });
    };
    CadastroComponent.prototype.updateCadastro = function (cadastro) {
        var _this = this;
        this.cadastroService.updateOrder(this.cadastroKey, cadastro)
            .subscribe(function (ans) {
            _this.toastr.success('Atualizado!', 'Sucesso!');
            _this.isLoading = true;
            _this.getCadastros();
        }, function (err) {
            _this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
        });
    };
    CadastroComponent.prototype.applyFilter = function (event) {
        var filterValue = event.target.value;
        this.cadastros.filter = filterValue.trim().toLowerCase();
        if (this.cadastros.paginator) {
            this.cadastros.paginator.firstPage();
        }
    };
    CadastroComponent.prototype.openDialog = function () {
        this.imgSrc = "";
        this.items().clear();
        this.addItem();
        this.controlSubmit = true;
        this.controlDelete = false;
        this.userform.reset();
        var dialogRef = this.dialog.open(this.customTemplate);
        // dialogRef.afterClosed().subscribe(result => {
        //   console.log(`Dialog result: ${result}`);
        // });
    };
    CadastroComponent.prototype.openDialogEdit = function (cadastro) {
        var _this = this;
        cadastro.data = cadastro.data.substring(0, 10);
        this.dataFormItem = cadastro;
        this.items().clear();
        this.dataFormItem.items.forEach(function () {
            _this.editItem();
        });
        var resultado = cadastro.evidencia == undefined ? '' : cadastro.evidencia.substring(5, 20);
        this.controlPreviewFormat = resultado == 'application/pdf' ? true : false;
        this.imgSrc = cadastro.evidencia;
        this.controlSubmit = false;
        this.cadastroKey = cadastro;
        this.controlDelete = true;
        this.userform.patchValue(cadastro);
        var dialogRef = this.dialog.open(this.customTemplate);
        // dialogRef.afterClosed().subscribe(result => {
        //   console.log(`Dialog result: ${result}`);
        // });
    };
    CadastroComponent.prototype.getCurrent = function (event) {
        var _this = this;
        this.isLoading = true;
        this.cadastroService.getOrders(event.pageIndex).subscribe(function (data) {
            var cadastros = data.cadastros;
            _this.cadastros = new _angular_material_table__WEBPACK_IMPORTED_MODULE_7__["MatTableDataSource"](cadastros);
            _this.isLoading = false;
        }, function (error) {
            _this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
        });
        // const skip = this.paginator.pageSize * this.paginator.pageIndex;
        // const paged = this.data.filter((u, i) => i >= skip)
        // .filter((u, i) => i <this.paginator.pageSize);
        // console.log(paged);
        // this.cadastros = new MatTableDataSource(paged);
    };
    CadastroComponent.ctorParameters = function () { return [
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
        { type: app_shared_services_cadastro_cadastro_service__WEBPACK_IMPORTED_MODULE_8__["CadastroService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_9__["ToastrService"] }
    ]; };
    CadastroComponent.propDecorators = {
        customTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"], args: ['myTemplate',] }],
        paginator: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"], args: [_angular_material_paginator__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"],] }],
        sort: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"], args: [_angular_material_sort__WEBPACK_IMPORTED_MODULE_6__["MatSort"],] }]
    };
    CadastroComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-cadastro',
            template: _raw_loader_cadastro_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_cadastro_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            app_shared_services_cadastro_cadastro_service__WEBPACK_IMPORTED_MODULE_8__["CadastroService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_9__["ToastrService"]])
    ], CadastroComponent);
    return CadastroComponent;
}());



/***/ }),

/***/ "in5m":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\n  <div class=\"container-fluid\">\n    <div class=\"login-page\">\n      <div class=\"row justify-content-md-center\">\n        <div class=\"col-md-4\">\n          <img src=\"../../assets/img/LOGO-SOS-AM-MAPA.png\" width=\"210px\" height=\"210px\" class=\"user-avatar\" />\n          <h5>SOS AMAZONAS</h5>\n          <form role=\"form\">\n            <div class=\"form-content\">\n              <div class=\"form-group\">\n                <input type=\"text\" ng-model=\"name\" class=\"form-control input-underline input-lg\" id=\"\"\n                  placeholder=\"E-mail\" #email>\n              </div>\n\n              <div class=\"form-group\">\n                <input type=\"password\" class=\"form-control input-underline input-lg\" id=\"\" placeholder=\"Senha\" #pwd>\n              </div>\n            </div>\n            <a class=\"btn rounded-btn\" (click)=\"onLoggedin(email, pwd)\"> Entrar </a>\n            <br>\n            <br>\n            <a class=\"btn rounded-btn\" (click)=\"forgetPwd(email)\">Esqueci minha senha</a>\n          </form>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n");

/***/ }),

/***/ "nod/":
/*!**************************************************!*\
  !*** ./src/app/not-found/not-found.component.ts ***!
  \**************************************************/
/*! exports provided: NotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundComponent", function() { return NotFoundComponent; });
/* harmony import */ var _raw_loader_not_found_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./not-found.component.html */ "s2In");
/* harmony import */ var _not_found_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./not-found.component.css */ "VziJ");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NotFoundComponent = /** @class */ (function () {
    function NotFoundComponent() {
    }
    NotFoundComponent.prototype.ngOnInit = function () {
    };
    NotFoundComponent.ctorParameters = function () { return []; };
    NotFoundComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-not-found',
            template: _raw_loader_not_found_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_not_found_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [])
    ], NotFoundComponent);
    return NotFoundComponent;
}());



/***/ }),

/***/ "qZ7x":
/*!**************************************************************!*\
  !*** ./src/app/layouts/admin-layout/admin-layout.routing.ts ***!
  \**************************************************************/
/*! exports provided: AdminLayoutRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminLayoutRoutes", function() { return AdminLayoutRoutes; });
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../dashboard/dashboard.component */ "QX6l");
/* harmony import */ var app_cadastro_cadastro_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/cadastro/cadastro.component */ "gK0J");
/* harmony import */ var app_login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/login/login.component */ "vtpD");
/* harmony import */ var app_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/not-found/not-found.component */ "nod/");
/* harmony import */ var app_auth_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/auth.guard */ "tIkO");





var AdminLayoutRoutes = [
    { path: 'cadastro', component: app_cadastro_cadastro_component__WEBPACK_IMPORTED_MODULE_1__["CadastroComponent"], canActivate: [app_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]] },
    { path: 'dashboard', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_0__["DashboardComponent"] },
    { path: 'login', component: app_login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"] },
    { path: 'not-found', component: app_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_3__["NotFoundComponent"] },
    { path: '**', redirectTo: 'not-found' },
];


/***/ }),

/***/ "rCWb":
/*!**************************************************!*\
  !*** ./src/app/cadastro/cadastro.component.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table {\n  width: 100%;\n}\n\n.button-cadastro-flutuante {\n  flex-direction: row-reverse;\n  z-index: 15000;\n  top: 50px;\n  /* altura que vai parar antes do topo */\n  position: -webkit-sticky;\n  position: sticky;\n}\n\n.mat-form-field {\n  font-size: 14px;\n  width: 100%;\n}\n\ntd,\nth {\n  width: 25%;\n}\n\n.custom-icon-add-item {\n  background-color: #999999;\n  line-height: 20px;\n  font: icon;\n}\n\n.custom-icon-add-item::ng-deep .mat-button-wrapper {\n  line-height: 25px;\n  padding: 0;\n}\n\n.custom-icon-add-item::ng-deep .mat-button-wrapper .mat-icon {\n  font-size: 20px;\n  padding-right: 0px;\n  padding-top: 3px;\n}\n\n.custom-icon-remove {\n  width: 35px;\n  height: 35px;\n  line-height: 20px;\n  font-size: 20px;\n  margin-bottom: 30px !important;\n}\n\n.custom-icon-remove::ng-deep .mat-button-wrapper {\n  line-height: 25px;\n  padding: 0;\n}\n\n.custom-icon-remove::ng-deep .mat-button-wrapper .mat-icon {\n  font-size: 20px;\n  padding-right: 0px;\n  padding-top: 3px;\n}\n\n.custom-icon-add {\n  width: 35px;\n  height: 35px;\n  line-height: 20px;\n  font-size: 20px;\n}\n\n.custom-icon-add::ng-deep .mat-button-wrapper {\n  line-height: 25px;\n  padding: 0;\n}\n\n.custom-icon-add::ng-deep .mat-button-wrapper .mat-icon {\n  font-size: 20px;\n  padding-right: 0px;\n  padding-top: 3px;\n}\n\n.my-fab {\n  width: 30px;\n  height: 30px;\n  line-height: 20px;\n  font-size: 20px;\n}\n\n.my-fab::ng-deep .mat-button-wrapper {\n  line-height: 20px;\n  padding: 0;\n}\n\n.my-fab::ng-deep .mat-button-wrapper .mat-icon {\n  font-size: 17px;\n  padding-right: 0px;\n  padding-top: 4px;\n}\n\n@media only screen and (max-width: 415px) {\n  .img-cadastro-moble {\n    width: 100% !important;\n  }\n}\n\n/* Structure */\n\n.example-container {\n  position: relative;\n  min-height: 200px;\n}\n\n.example-table-container {\n  position: relative;\n  overflow: auto;\n}\n\n.example-loading-shade {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 56px;\n  right: 0;\n  background: rgba(0, 0, 0, 0.15);\n  z-index: 1;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.example-rate-limit-reached {\n  color: #980000;\n  max-width: 360px;\n  text-align: center;\n}\n\n/* Column Widths */\n\n.mat-column-number,\n.mat-column-state {\n  max-width: 64px;\n}\n\n.mat-column-created {\n  max-width: 124px;\n}\n\ntd.mat-column-star {\n  width: 20px;\n  padding-right: 8px !important;\n}\n\n.mat-row:hover {\n  background-color: #575757;\n}\n\n.mat-row a {\n  color: #3e75ff;\n  text-decoration: none;\n}\n\n.mat-row a:hover {\n  text-decoration: underline;\n}\n\n.mat-form-field {\n  font-size: 14px;\n  width: 100%;\n}\n\n@media screen and (max-width: 960px) {\n  .mat-elevation-z8 {\n    background: transparent;\n    box-shadow: none;\n  }\n\n  .mat-header-row {\n    display: none;\n  }\n\n  tbody {\n    display: block;\n    width: 100%;\n  }\n\n  .mat-table {\n    background: transparent;\n  }\n  .mat-table * {\n    box-sizing: border-box;\n  }\n  .mat-table .mat-row {\n    display: block;\n    overflow: hidden;\n    height: auto;\n    position: relative;\n    clear: both;\n    box-shadow: 0 2px 1px -1px rgba(0, 0, 0, 0.2), 0 1px 1px 0 rgba(0, 0, 0, 0.14), 0 1px 3px 0 rgba(0, 0, 0, 0.12);\n    border-radius: 3px;\n  }\n  .mat-table .mat-row + .mat-row {\n    margin-top: 24px;\n  }\n  .mat-table .mat-cell {\n    display: block;\n    width: 100%;\n    padding: 0 16px;\n    margin: 16px 0;\n    border: 0 none;\n  }\n  .mat-table .mat-cell:first-child {\n    padding: 0 48px 0 16px;\n  }\n  .mat-table .mat-cell:first-child a {\n    font-size: 20px;\n    color: inherit;\n  }\n  .mat-table .mat-cell:first-child:before {\n    display: none;\n  }\n  .mat-table .mat-cell.m-card-sub-title {\n    margin-top: -8px;\n    padding: 0 48px 0 16px;\n    color: rgba(0, 0, 0, 0.54);\n  }\n  .mat-table .has_label_on_mobile:before {\n    content: attr(data-label);\n    display: inline;\n    font-weight: normal;\n  }\n  .mat-table .mat-column-star {\n    width: auto;\n    padding: 8px 0 0 !important;\n    margin: 0;\n    position: absolute;\n    top: 0;\n    right: 0;\n  }\n  .mat-table .mat-column-star:before {\n    display: none;\n  }\n\n  .mat-paginator {\n    margin-top: 24px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FkYXN0cm8vY2FkYXN0cm8uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFBO0FBQ0Y7O0FBRUE7RUFDRSwyQkFBQTtFQUNBLGNBQUE7RUFDQSxTQUFBO0VBQVcsdUNBQUE7RUFDWCx3QkFBQTtFQUFBLGdCQUFBO0FBRUY7O0FBQ0E7RUFDRSxlQUFBO0VBQ0EsV0FBQTtBQUVGOztBQUNBOztFQUVFLFVBQUE7QUFFRjs7QUFDQTtFQUdFLHlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxVQUFBO0FBQUY7O0FBRUU7RUFDRSxpQkFBQTtFQUNBLFVBQUE7QUFBSjs7QUFFSTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBQU47O0FBS0E7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLDhCQUFBO0FBRkY7O0FBSUU7RUFDRSxpQkFBQTtFQUNBLFVBQUE7QUFGSjs7QUFJSTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBRk47O0FBTUE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQUhGOztBQUtFO0VBQ0UsaUJBQUE7RUFDQSxVQUFBO0FBSEo7O0FBS0k7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQUhOOztBQU9BO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUFKRjs7QUFNRTtFQUNFLGlCQUFBO0VBQ0EsVUFBQTtBQUpKOztBQU1JO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUFKTjs7QUFTQTtFQUNFO0lBQ0Usc0JBQUE7RUFORjtBQUNGOztBQVFBLGNBQUE7O0FBQ0E7RUFDRSxrQkFBQTtFQUNBLGlCQUFBO0FBTkY7O0FBU0E7RUFDRSxrQkFBQTtFQUVBLGNBQUE7QUFQRjs7QUFVQTtFQUNFLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxZQUFBO0VBQ0EsUUFBQTtFQUNBLCtCQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBUEY7O0FBVUE7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQVBGOztBQVVBLGtCQUFBOztBQUNBOztFQUVFLGVBQUE7QUFQRjs7QUFVQTtFQUNFLGdCQUFBO0FBUEY7O0FBWUE7RUFDRSxXQUFBO0VBQ0EsNkJBQUE7QUFURjs7QUFhRTtFQUNFLHlCQUFBO0FBVko7O0FBWUU7RUFDRSxjQUFBO0VBQ0EscUJBQUE7QUFWSjs7QUFXSTtFQUNFLDBCQUFBO0FBVE47O0FBY0E7RUFDRSxlQUFBO0VBQ0EsV0FBQTtBQVhGOztBQWNBO0VBQ0U7SUFDRSx1QkFBQTtJQUNBLGdCQUFBO0VBWEY7O0VBY0E7SUFDRSxhQUFBO0VBWEY7O0VBY0E7SUFDRSxjQUFBO0lBQ0EsV0FBQTtFQVhGOztFQWNBO0lBQ0UsdUJBQUE7RUFYRjtFQVlFO0lBQ0Usc0JBQUE7RUFWSjtFQWFFO0lBQ0UsY0FBQTtJQUNBLGdCQUFBO0lBQ0EsWUFBQTtJQUNBLGtCQUFBO0lBQ0EsV0FBQTtJQUNBLCtHQUFBO0lBQ0Esa0JBQUE7RUFYSjtFQVlJO0lBQ0UsZ0JBQUE7RUFWTjtFQWNFO0lBQ0UsY0FBQTtJQUNBLFdBQUE7SUFDQSxlQUFBO0lBQ0EsY0FBQTtJQUNBLGNBQUE7RUFaSjtFQWFJO0lBQ0Usc0JBQUE7RUFYTjtFQVlNO0lBQ0UsZUFBQTtJQUVBLGNBQUE7RUFYUjtFQWFNO0lBQ0UsYUFBQTtFQVhSO0VBY0k7SUFDRSxnQkFBQTtJQUNBLHNCQUFBO0lBQ0EsMEJBQUE7RUFaTjtFQWlCSTtJQUNFLHlCQUFBO0lBQ0EsZUFBQTtJQUNBLG1CQUFBO0VBZk47RUFtQkU7SUFDRSxXQUFBO0lBQ0EsMkJBQUE7SUFDQSxTQUFBO0lBQ0Esa0JBQUE7SUFDQSxNQUFBO0lBQ0EsUUFBQTtFQWpCSjtFQWtCSTtJQUNFLGFBQUE7RUFoQk47O0VBcUJBO0lBQ0UsZ0JBQUE7RUFsQkY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL2NhZGFzdHJvL2NhZGFzdHJvLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmJ1dHRvbi1jYWRhc3Ryby1mbHV0dWFudGUge1xuICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XG4gIHotaW5kZXg6IDE1MDAwO1xuICB0b3A6IDUwcHg7IC8qIGFsdHVyYSBxdWUgdmFpIHBhcmFyIGFudGVzIGRvIHRvcG8gKi9cbiAgcG9zaXRpb246IHN0aWNreTtcbn1cblxuLm1hdC1mb3JtLWZpZWxkIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICB3aWR0aDogMTAwJTtcbn1cblxudGQsXG50aCB7XG4gIHdpZHRoOiAyNSU7XG59XG5cbi5jdXN0b20taWNvbi1hZGQtaXRlbSB7XG4gIC8vIHdpZHRoOiAzNXB4O1xuICAvLyBoZWlnaHQ6IDM1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM5OTk5OTk7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBmb250OiBpY29uO1xuXG4gICY6Om5nLWRlZXAgLm1hdC1idXR0b24td3JhcHBlciB7XG4gICAgbGluZS1oZWlnaHQ6IDI1cHg7XG4gICAgcGFkZGluZzogMDtcblxuICAgIC5tYXQtaWNvbiB7XG4gICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG4gICAgICBwYWRkaW5nLXRvcDogM3B4O1xuICAgIH1cbiAgfVxufVxuXG4uY3VzdG9tLWljb24tcmVtb3ZlIHtcbiAgd2lkdGg6IDM1cHg7XG4gIGhlaWdodDogMzVweDtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbWFyZ2luLWJvdHRvbTogMzBweCAhaW1wb3J0YW50O1xuXG4gICY6Om5nLWRlZXAgLm1hdC1idXR0b24td3JhcHBlciB7XG4gICAgbGluZS1oZWlnaHQ6IDI1cHg7XG4gICAgcGFkZGluZzogMDtcblxuICAgIC5tYXQtaWNvbiB7XG4gICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG4gICAgICBwYWRkaW5nLXRvcDogM3B4O1xuICAgIH1cbiAgfVxufVxuLmN1c3RvbS1pY29uLWFkZCB7XG4gIHdpZHRoOiAzNXB4O1xuICBoZWlnaHQ6IDM1cHg7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBmb250LXNpemU6IDIwcHg7XG5cbiAgJjo6bmctZGVlcCAubWF0LWJ1dHRvbi13cmFwcGVyIHtcbiAgICBsaW5lLWhlaWdodDogMjVweDtcbiAgICBwYWRkaW5nOiAwO1xuXG4gICAgLm1hdC1pY29uIHtcbiAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcbiAgICAgIHBhZGRpbmctdG9wOiAzcHg7XG4gICAgfVxuICB9XG59XG4ubXktZmFiIHtcbiAgd2lkdGg6IDMwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIGZvbnQtc2l6ZTogMjBweDtcblxuICAmOjpuZy1kZWVwIC5tYXQtYnV0dG9uLXdyYXBwZXIge1xuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgIHBhZGRpbmc6IDA7XG5cbiAgICAubWF0LWljb24ge1xuICAgICAgZm9udC1zaXplOiAxN3B4O1xuICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xuICAgICAgcGFkZGluZy10b3A6IDRweDtcbiAgICB9XG4gIH1cbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0MTVweCkge1xuICAuaW1nLWNhZGFzdHJvLW1vYmxlIHtcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICB9XG59XG4vKiBTdHJ1Y3R1cmUgKi9cbi5leGFtcGxlLWNvbnRhaW5lciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWluLWhlaWdodDogMjAwcHg7XG59XG5cbi5leGFtcGxlLXRhYmxlLWNvbnRhaW5lciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgLy8gbWF4LWhlaWdodDogNTAwcHg7XG4gIG92ZXJmbG93OiBhdXRvO1xufVxuXG4uZXhhbXBsZS1sb2FkaW5nLXNoYWRlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGJvdHRvbTogNTZweDtcbiAgcmlnaHQ6IDA7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4xNSk7XG4gIHotaW5kZXg6IDE7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uZXhhbXBsZS1yYXRlLWxpbWl0LXJlYWNoZWQge1xuICBjb2xvcjogIzk4MDAwMDtcbiAgbWF4LXdpZHRoOiAzNjBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4vKiBDb2x1bW4gV2lkdGhzICovXG4ubWF0LWNvbHVtbi1udW1iZXIsXG4ubWF0LWNvbHVtbi1zdGF0ZSB7XG4gIG1heC13aWR0aDogNjRweDtcbn1cblxuLm1hdC1jb2x1bW4tY3JlYXRlZCB7XG4gIG1heC13aWR0aDogMTI0cHg7XG59XG5cbi8vVlZWVlZWVlZWIFJlc3BvbnNpdmUgQ1NTIE1vYmlsZSBGYWJpbyBXZW5kZWwgVlZWVlZWVlZWVlxuXG50ZC5tYXQtY29sdW1uLXN0YXIge1xuICB3aWR0aDogMjBweDtcbiAgcGFkZGluZy1yaWdodDogOHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5tYXQtcm93IHtcbiAgJjpob3ZlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzU3NTc1NztcbiAgfVxuICBhIHtcbiAgICBjb2xvcjogIzNlNzVmZjtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgJjpob3ZlciB7XG4gICAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbiAgICB9XG4gIH1cbn1cblxuLm1hdC1mb3JtLWZpZWxkIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICB3aWR0aDogMTAwJTtcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTYwcHgpIHtcbiAgLm1hdC1lbGV2YXRpb24tejgge1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gIH1cblxuICAubWF0LWhlYWRlci1yb3cge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICB0Ym9keSB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cblxuICAubWF0LXRhYmxlIHtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAqIHtcbiAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgfVxuXG4gICAgLm1hdC1yb3cge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgICAgaGVpZ2h0OiBhdXRvO1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgY2xlYXI6IGJvdGg7XG4gICAgICBib3gtc2hhZG93OiAwIDJweCAxcHggLTFweCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgMXB4IDFweCAwIHJnYmEoMCwgMCwgMCwgMC4xNCksIDAgMXB4IDNweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gICAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICArIC5tYXQtcm93IHtcbiAgICAgICAgbWFyZ2luLXRvcDogMjRweDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAubWF0LWNlbGwge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIHBhZGRpbmc6IDAgMTZweDtcbiAgICAgIG1hcmdpbjogMTZweCAwO1xuICAgICAgYm9yZGVyOiAwIG5vbmU7XG4gICAgICAmOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgcGFkZGluZzogMCA0OHB4IDAgMTZweDtcbiAgICAgICAgYSB7XG4gICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgIC8vIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgICAgY29sb3I6IGluaGVyaXQ7XG4gICAgICAgIH1cbiAgICAgICAgJjpiZWZvcmUge1xuICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgICYubS1jYXJkLXN1Yi10aXRsZSB7XG4gICAgICAgIG1hcmdpbi10b3A6IC04cHg7XG4gICAgICAgIHBhZGRpbmc6IDAgNDhweCAwIDE2cHg7XG4gICAgICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNTQpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC5oYXNfbGFiZWxfb25fbW9iaWxlIHtcbiAgICAgICY6YmVmb3JlIHtcbiAgICAgICAgY29udGVudDogYXR0cihkYXRhLWxhYmVsKTtcbiAgICAgICAgZGlzcGxheTogaW5saW5lO1xuICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgICAgfVxuICAgIH1cblxuICAgIC5tYXQtY29sdW1uLXN0YXIge1xuICAgICAgd2lkdGg6IGF1dG87XG4gICAgICBwYWRkaW5nOiA4cHggMCAwICFpbXBvcnRhbnQ7XG4gICAgICBtYXJnaW46IDA7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICB0b3A6IDA7XG4gICAgICByaWdodDogMDtcbiAgICAgICY6YmVmb3JlIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAubWF0LXBhZ2luYXRvciB7XG4gICAgbWFyZ2luLXRvcDogMjRweDtcbiAgfVxufVxuIl19 */");

/***/ }),

/***/ "s2In":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/not-found/not-found.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\n  <div class=\"container-fluid d-flex justify-content-center\">\n    <img src=\"../../assets/img/404-not-found.png\">\n  </div>\n</div>");

/***/ }),

/***/ "tIkO":
/*!*******************************!*\
  !*** ./src/app/auth.guard.ts ***!
  \*******************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        return this.checkUserLogin();
    };
    AuthGuard.prototype.canActivateChild = function (childRoute, state) {
        return true;
    };
    AuthGuard.prototype.canDeactivate = function (component, currentRoute, currentState, nextState) {
        return true;
    };
    AuthGuard.prototype.canLoad = function (route, segments) {
        return true;
    };
    AuthGuard.prototype.checkUserLogin = function () {
        if (localStorage.getItem('isLoggedin')) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    };
    AuthGuard.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
    ]; };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "vtpD":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _raw_loader_login_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./login.component.html */ "in5m");
/* harmony import */ var _login_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.component.scss */ "ZTFi");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var app_shared_services_login_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/services/login/login.service */ "EVec");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, loginService, toastr) {
        this.router = router;
        this.loginService = loginService;
        this.toastr = toastr;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.onLoggedin = function (email, pwd) {
        var _this = this;
        this.loginService.login(email, pwd).then(function (ok) {
            localStorage.setItem('isLoggedin', 'true');
            _this.router.navigate(["/cadastro"]);
        }).catch(function (error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            switch (errorCode) {
                case 'auth/wrong-password': {
                    _this.toastr.error('Senha inválida, tente novamente', 'Erro!');
                    return;
                }
                case 'auth/invalid-email': {
                    _this.toastr.error("Usu\u00E1rio inv\u00E1lido.\n" + email.value + ", tente novamente'", 'Erro!');
                    return;
                }
                case 'auth/user-disabled': {
                    _this.toastr.error('Usuário desabilitado.', 'Erro!');
                    return;
                }
                case 'auth/user-not-found': {
                    _this.toastr.error("Usu\u00E1rio n\u00E3o encontrado.\n" + email.value, 'Erro!');
                    return;
                }
                default: _this.toastr.error('Usuário não encontrado. ' + errorMessage, 'Erro!');
            }
        });
    };
    LoginComponent.prototype.forgetPwd = function (email) {
        var _this = this;
        if (!email.value) {
            this.toastr.error('Informe seu e-mail.', 'Erro!');
            return;
        }
        this.loginService.resetPwd(email.value).then(function () {
            _this.toastr.success('Foi enviado um e-mail para ' + email.value + ' com instruções para redefinir a sua senha.', 'Sucesso!');
        }).catch(function (error) {
            _this.toastr.error('Não foi possível enviar e-mail, entre em contato com o administrador do sistema.', 'Erro!');
        });
    };
    LoginComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: app_shared_services_login_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"] }
    ]; };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-login',
            template: _raw_loader_login_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_login_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            app_shared_services_login_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ })

}]);
//# sourceMappingURL=layouts-admin-layout-admin-layout-module.js.map