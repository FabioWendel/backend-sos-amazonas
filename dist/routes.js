"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _express = require('express');
// import multer from 'multer';

// import multerConfig from './config/multer';

var _OrderController = require('./app/controllers/OrderController'); var _OrderController2 = _interopRequireDefault(_OrderController);
var _ItemController = require('./app/controllers/ItemController'); var _ItemController2 = _interopRequireDefault(_ItemController);

// import FileController from './app/controllers/FileController';

const routes = new (0, _express.Router)();
// const upload = multer(multerConfig);

// routes.post('/files/:code/photo', upload.single('file'), FileController.store);

/**
 *
 */
routes.get('/orders', _OrderController2.default.index);
routes.get('/orders/:id', _OrderController2.default.show);
routes.post('/orders', _OrderController2.default.store);
routes.put('/orders/:id', _OrderController2.default.update);
routes.delete('/orders/:id', _OrderController2.default.delete);

routes.get('/items', _ItemController2.default.index);
routes.get('/items/:id', _ItemController2.default.show);
routes.get('/local/', _ItemController2.default.showAllLocal);
routes.get('/local/:id', _ItemController2.default.showLocal);
routes.get('/allitems/', _ItemController2.default.showAllItems);
routes.get('/allitems/:id', _ItemController2.default.showAllByItems);

exports. default = routes;
