"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);

var _Order = require('../models/Order'); var _Order2 = _interopRequireDefault(_Order);
var _Item = require('../models/Item'); var _Item2 = _interopRequireDefault(_Item);

class OrderController {
  async store(req, res) {
    const schema = Yup.object().shape({
      termo: Yup.string().required(),
      data: Yup.date().required(),
      municipio: Yup.string().required(),
      local: Yup.string().required(),
      items: Yup.array().required(),
      evidencia: Yup.string().nullable(),
      observacao: Yup.string().nullable(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const cadastroExiste = await _Order2.default.findOne({
      where: { termo: req.body.termo },
    });

    if (cadastroExiste) {
      return res.status(400).json({ error: 'Termo already exists' });
    }

    const {
      items,
      evidencia,
      local,
      municipio,
      termo,
      data,
      observacao,
    } = req.body;

    const order = await _Order2.default.create({
      evidencia,
      local,
      municipio,
      termo,
      data,
      observacao,
    });

    const savedItems = await _Item2.default.bulkCreate(
      items.map(el => ({ ...el, order_id: order.id }))
    );
    return res.json({ order, items: savedItems });
  }

  async index(req, res) {
    const { page = 0, quantity = 5 } = req.query;

    const { rows: cadastros, count } = await _Order2.default.findAndCountAll({
      order: [['updatedAt', 'DESC']],
      limit: quantity,
      offset: (page - 0) * quantity,
      include: [
        {
          model: _Item2.default,
          as: 'items',
        },
      ],
    });
    return res.json({
      cadastros,
      count,
      totalPages: Math.ceil(count / quantity),
    });

    // const cadastros = await Order.findAll({
    //   include: [
    //     {
    //       model: Item,
    //       as: 'items',
    //     },
    //   ],
    // });

    // return res.json(cadastros);
  }

  async show(req, res) {
    const { id } = req.params;
    const cadastro = await _Order2.default.findOne({
      where: {
        termo: id,
      },
    });

    if (!cadastro) {
      return res.status(400).json({ error: 'Cadastro not exists.' });
    }

    return res.json(cadastro);
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      termo: Yup.string().required(),
      data: Yup.string().required(),
      municipio: Yup.string().required(),
      local: Yup.string().required(),
      items: Yup.string().required(),
      evidencia: Yup.string().nullable(),
      observacao: Yup.string().nullable(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(401).json({ error: 'Validation fails' });
    }

    const order = await _Order2.default.findOne({
      where: { id: req.params.id },
    });

    if (!order) {
      return res.status(400).json({ error: 'Termo not found.' });
    }

    const { items, ...rest } = req.body;

    const {
      evidencia,
      local,
      municipio,
      termo,
      data,
      observacao,
    } = await order.update(rest);

    await order.save();

    const updatedItems = [];

    await _Item2.default.destroy({ where: { order_id: order.id } });

    const promises = items.map(async el => {
      const item = await _Item2.default.create({ ...el, order_id: order.id });
      updatedItems.push(item);
    });

    await Promise.all(promises);

    return res.json({
      evidencia,
      local,
      municipio,
      termo,
      data,
      observacao,
      items: updatedItems,
    });
  }

  async delete(req, res) {
    const { id } = req.params;

    const cadastro = await _Order2.default.findOne({
      where: { id },
    });

    if (!cadastro) {
      return res.status(401).json({ error: 'cadastro not found.' });
    }

    // await Item.destroy({ where: { order_id: cadastro.id } });

    await cadastro.destroy({ where: { id } });

    return res.status(200).json();
  }
}

exports. default = new OrderController();
