"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize');

var _Item = require('../models/Item'); var _Item2 = _interopRequireDefault(_Item);
var _Order = require('../models/Order'); var _Order2 = _interopRequireDefault(_Order);

class ItemController {
  async store(req, res) {
    return res.json({ ok: 'true' });
  }

  async index(req, res) {
    // const items = await Item.findAll({
    //   attributes: ['name', [fn('sum', col('quantidade')), 'totalItems']],
    //   include: [
    //     {
    //       model: Order,
    //       as: 'order',
    //       // attributes: ['municipio'],
    //       attributes: [],
    //     },
    //   ],
    //   group: ['name'],
    //   raw: true,
    //   order: literal('totalItems DESC'),
    // });

    const itemsMunicipio = await _Order2.default.findAll({
      attributes: [
        'municipio',
        [_sequelize.fn.call(void 0, 'count', _sequelize.col.call(void 0, 'municipio')), 'totalItemsDoados'],
      ],
      include: [
        {
          model: _Item2.default,
          as: 'items',
          // attributes: ['municipio'],
          attributes: [],
        },
      ],
      group: ['municipio'],
      raw: true,
      order: _sequelize.literal.call(void 0, 'totalItemsDoados DESC'),
    });

    return res.json({ municipio: itemsMunicipio });
  }

  async show(req, res) {
    const { id } = req.params;

    const items = await _Item2.default.findAll({
      attributes: [
        'name',
        'order.municipio',
        'order.local',
        'medida',
        [_sequelize.fn.call(void 0, 'sum', _sequelize.col.call(void 0, 'quantidade')), 'totalItems'],
      ],
      include: [
        {
          model: _Order2.default,
          as: 'order',
          where: {
            municipio: id,
          },
          attributes: [],
        },
      ],
      group: ['name', 'local', 'medida', 'municipio'],
      raw: true,
      order: _sequelize.literal.call(void 0, 'totalItems DESC'),
    });

    if (!items) {
      return res.status(400).json({ error: 'Item not exists.' });
    }

    return res.json(items);
  }

  async showLocal(req, res) {
    const { id } = req.params;

    const items = await _Item2.default.findAll({
      attributes: [
        'name',
        'order.municipio',
        'order.local',
        'medida',
        [_sequelize.fn.call(void 0, 'sum', _sequelize.col.call(void 0, 'quantidade')), 'totalItems'],
      ],
      include: [
        {
          model: _Order2.default,
          as: 'order',
          where: {
            local: id,
          },
          attributes: [],
        },
      ],
      group: ['name', 'local', 'municipio', 'medida'],
      raw: true,
      order: _sequelize.literal.call(void 0, 'totalItems DESC'),
    });

    if (!items) {
      return res.status(400).json({ error: 'Item not exists.' });
    }

    return res.json(items);
  }

  async showAllItems(req, res) {
    const items = await _Item2.default.findAll({
      attributes: ['name'],
      group: ['name'],
      raw: true,
    });

    return res.json(items);
  }

  async showAllLocal(req, res) {
    const items = await _Order2.default.findAll({
      attributes: ['local'],
      group: ['local'],
      raw: true,
    });

    return res.json(items);
  }

  async showAllByItems(req, res) {
    const { id } = req.params;

    const items = await _Item2.default.findAll({
      where: {
        name: id,
      },
      attributes: [
        'name',
        'order.municipio',
        'order.local',
        'medida',
        [_sequelize.fn.call(void 0, 'sum', _sequelize.col.call(void 0, 'quantidade')), 'totalItems'],
      ],
      include: [
        {
          model: _Order2.default,
          as: 'order',
          // where: {
          //   local: id,
          // },
          attributes: [],
        },
      ],
      group: ['name', 'local', 'municipio', 'medida'],
      raw: true,
      order: _sequelize.literal.call(void 0, 'totalItems DESC'),
    });

    if (!items) {
      return res.status(400).json({ error: 'Item not exists.' });
    }

    return res.json(items);
  }

  async update(req, res) {
    return res.json({ ok: 'true' });
  }

  async delete(req, res) {
    return res.status(200).json();
  }
}

exports. default = new ItemController();
