"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class Order extends _sequelize.Model {
  static init(sequelize) {
    super.init(
      {
        data: _sequelize2.default.DATE,
        evidencia: _sequelize2.default.STRING,
        local: _sequelize2.default.STRING,
        municipio: _sequelize2.default.STRING,
        termo: _sequelize2.default.STRING,
        observacao: _sequelize2.default.STRING,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.hasMany(models.Item, {
      foreignKey: 'order_id',
      as: 'orders',
    });

    this.hasMany(models.Item, {
      foreignKey: 'order_id',
      as: 'items',
    });
  }
}

exports. default = Order;
