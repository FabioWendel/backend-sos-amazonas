import { Router } from 'express';
// import multer from 'multer';

// import multerConfig from './config/multer';

import OrderController from './app/controllers/OrderController';
import ItemController from './app/controllers/ItemController';

// import FileController from './app/controllers/FileController';

const routes = new Router();
// const upload = multer(multerConfig);

// routes.post('/files/:code/photo', upload.single('file'), FileController.store);

/**
 *
 */
routes.get('/orders', OrderController.index);
routes.get('/orders/:id', OrderController.show);
routes.post('/orders', OrderController.store);
routes.put('/orders/:id', OrderController.update);
routes.delete('/orders/:id', OrderController.delete);

routes.get('/items', ItemController.index);
routes.get('/items/:id', ItemController.show);
routes.get('/local/', ItemController.showAllLocal);
routes.get('/local/:id', ItemController.showLocal);
routes.get('/allitems/', ItemController.showAllItems);
routes.get('/allitems/:id', ItemController.showAllByItems);

export default routes;
