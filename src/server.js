import app from './app';

app.listen(process.env.PORT || 3333, () => {
  console.log('PORTA =>', process.env.PORT);
  console.log('Started backend...');
});
