import Sequelize, { Model } from 'sequelize';

class Order extends Model {
  static init(sequelize) {
    super.init(
      {
        data: Sequelize.DATE,
        evidencia: Sequelize.STRING,
        local: Sequelize.STRING,
        municipio: Sequelize.STRING,
        termo: Sequelize.STRING,
        observacao: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.hasMany(models.Item, {
      foreignKey: 'order_id',
      as: 'orders',
    });

    this.hasMany(models.Item, {
      foreignKey: 'order_id',
      as: 'items',
    });
  }
}

export default Order;
