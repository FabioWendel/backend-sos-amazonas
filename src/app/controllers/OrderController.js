import * as Yup from 'yup';

import Order from '../models/Order';
import Item from '../models/Item';

class OrderController {
  async store(req, res) {
    const schema = Yup.object().shape({
      termo: Yup.string().required(),
      data: Yup.date().required(),
      municipio: Yup.string().required(),
      local: Yup.string().required(),
      items: Yup.array().required(),
      evidencia: Yup.string().nullable(),
      observacao: Yup.string().nullable(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const cadastroExiste = await Order.findOne({
      where: { termo: req.body.termo },
    });

    if (cadastroExiste) {
      return res.status(400).json({ error: 'Termo already exists' });
    }

    const {
      items,
      evidencia,
      local,
      municipio,
      termo,
      data,
      observacao,
    } = req.body;

    const order = await Order.create({
      evidencia,
      local,
      municipio,
      termo,
      data,
      observacao,
    });

    const savedItems = await Item.bulkCreate(
      items.map(el => ({ ...el, order_id: order.id }))
    );
    return res.json({ order, items: savedItems });
  }

  async index(req, res) {
    const { page = 0, quantity = 5 } = req.query;

    const { rows: cadastros, count } = await Order.findAndCountAll({
      order: [['updatedAt', 'DESC']],
      limit: quantity,
      offset: (page - 0) * quantity,
      include: [
        {
          model: Item,
          as: 'items',
        },
      ],
    });
    return res.json({
      cadastros,
      count,
      totalPages: Math.ceil(count / quantity),
    });

    // const cadastros = await Order.findAll({
    //   include: [
    //     {
    //       model: Item,
    //       as: 'items',
    //     },
    //   ],
    // });

    // return res.json(cadastros);
  }

  async show(req, res) {
    const { id } = req.params;
    const cadastro = await Order.findOne({
      where: {
        termo: id,
      },
    });

    if (!cadastro) {
      return res.status(400).json({ error: 'Cadastro not exists.' });
    }

    return res.json(cadastro);
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      termo: Yup.string().required(),
      data: Yup.string().required(),
      municipio: Yup.string().required(),
      local: Yup.string().required(),
      items: Yup.string().required(),
      evidencia: Yup.string().nullable(),
      observacao: Yup.string().nullable(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(401).json({ error: 'Validation fails' });
    }

    const order = await Order.findOne({
      where: { id: req.params.id },
    });

    if (!order) {
      return res.status(400).json({ error: 'Termo not found.' });
    }

    const { items, ...rest } = req.body;

    const {
      evidencia,
      local,
      municipio,
      termo,
      data,
      observacao,
    } = await order.update(rest);

    await order.save();

    const updatedItems = [];

    await Item.destroy({ where: { order_id: order.id } });

    const promises = items.map(async el => {
      const item = await Item.create({ ...el, order_id: order.id });
      updatedItems.push(item);
    });

    await Promise.all(promises);

    return res.json({
      evidencia,
      local,
      municipio,
      termo,
      data,
      observacao,
      items: updatedItems,
    });
  }

  async delete(req, res) {
    const { id } = req.params;

    const cadastro = await Order.findOne({
      where: { id },
    });

    if (!cadastro) {
      return res.status(401).json({ error: 'cadastro not found.' });
    }

    // await Item.destroy({ where: { order_id: cadastro.id } });

    await cadastro.destroy({ where: { id } });

    return res.status(200).json();
  }
}

export default new OrderController();
