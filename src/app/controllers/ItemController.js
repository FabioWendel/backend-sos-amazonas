import { fn, col, literal } from 'sequelize';

import Item from '../models/Item';
import Order from '../models/Order';

class ItemController {
  async store(req, res) {
    return res.json({ ok: 'true' });
  }

  async index(req, res) {
    // const items = await Item.findAll({
    //   attributes: ['name', [fn('sum', col('quantidade')), 'totalItems']],
    //   include: [
    //     {
    //       model: Order,
    //       as: 'order',
    //       // attributes: ['municipio'],
    //       attributes: [],
    //     },
    //   ],
    //   group: ['name'],
    //   raw: true,
    //   order: literal('totalItems DESC'),
    // });

    const itemsMunicipio = await Order.findAll({
      attributes: [
        'municipio',
        [fn('count', col('municipio')), 'totalItemsDoados'],
      ],
      include: [
        {
          model: Item,
          as: 'items',
          // attributes: ['municipio'],
          attributes: [],
        },
      ],
      group: ['municipio'],
      raw: true,
      order: literal('totalItemsDoados DESC'),
    });

    return res.json({ municipio: itemsMunicipio });
  }

  async show(req, res) {
    const { id } = req.params;

    const items = await Item.findAll({
      attributes: [
        'name',
        'order.municipio',
        'order.local',
        'medida',
        [fn('sum', col('quantidade')), 'totalItems'],
      ],
      include: [
        {
          model: Order,
          as: 'order',
          where: {
            municipio: id,
          },
          attributes: [],
        },
      ],
      group: ['name', 'local', 'medida', 'municipio'],
      raw: true,
      order: literal('totalItems DESC'),
    });

    if (!items) {
      return res.status(400).json({ error: 'Item not exists.' });
    }

    return res.json(items);
  }

  async showLocal(req, res) {
    const { id } = req.params;

    const items = await Item.findAll({
      attributes: [
        'name',
        'order.municipio',
        'order.local',
        'medida',
        [fn('sum', col('quantidade')), 'totalItems'],
      ],
      include: [
        {
          model: Order,
          as: 'order',
          where: {
            local: id,
          },
          attributes: [],
        },
      ],
      group: ['name', 'local', 'municipio', 'medida'],
      raw: true,
      order: literal('totalItems DESC'),
    });

    if (!items) {
      return res.status(400).json({ error: 'Item not exists.' });
    }

    return res.json(items);
  }

  async showAllItems(req, res) {
    const items = await Item.findAll({
      attributes: ['name'],
      group: ['name'],
      raw: true,
    });

    return res.json(items);
  }

  async showAllLocal(req, res) {
    const items = await Order.findAll({
      attributes: ['local'],
      group: ['local'],
      raw: true,
    });

    return res.json(items);
  }

  async showAllByItems(req, res) {
    const { id } = req.params;

    const items = await Item.findAll({
      where: {
        name: id,
      },
      attributes: [
        'name',
        'order.municipio',
        'order.local',
        'medida',
        [fn('sum', col('quantidade')), 'totalItems'],
      ],
      include: [
        {
          model: Order,
          as: 'order',
          // where: {
          //   local: id,
          // },
          attributes: [],
        },
      ],
      group: ['name', 'local', 'municipio', 'medida'],
      raw: true,
      order: literal('totalItems DESC'),
    });

    if (!items) {
      return res.status(400).json({ error: 'Item not exists.' });
    }

    return res.json(items);
  }

  async update(req, res) {
    return res.json({ ok: 'true' });
  }

  async delete(req, res) {
    return res.status(200).json();
  }
}

export default new ItemController();
